from aiohttp import web
import socketio
import asyncpg
import asyncio
MAX_LENGTH_FILE = 20971520  # 1024*1024*20

user = 'heaven_life'
database = 'heaven_life'
password = 'heaven_life'
host = 'localhost'
port = 45432
namespace = '/test'


async def init_app(loop):
    app = web.Application()
    sio = socketio.AsyncServer(async_mode='aiohttp')
    sio.attach(app)
    pool = await asyncpg.create_pool(user=user,
                                     database=database,
                                     password=password,
                                     host=host,
                                     port=port)

    sio.pool = pool
    app['pool'] = pool

    return app, sio

loop_ = asyncio.get_event_loop()
app_, sio_ = loop_.run_until_complete(init_app(loop_))


@sio_.on('connect', namespace=namespace)
async def test_connect(sid, environ):
    print('connect user')

    print(sio_.pool)

    await sio_.emit('my response',
                    {'data': 'Connected', 'count': 0},
                    room=sid,
                    namespace=namespace)


@sio_.on('conn', namespace=namespace)
async def connect(sid, message):
    print('my conn')
    print(sio_.pool)
    print(sid)
    sio_.emit('hello',{'data':'asdasd'}, room=sid,namespace=namespace)




web.run_app(app_,
            port=5005)
