import asyncio
from aiocache import caches, SimpleMemoryCache, MemcachedCache

caches.set_config({
    'default': {
        'cache': "aiocache.SimpleMemoryCache",
        'serializer': {
            'class': "aiocache.serializers.StringSerializer"
        }
    },
    'memcached_alt': {
        'cache': "aiocache.MemcachedCache",
        'endpoint': "127.0.0.1",
        'port': 22122,
        'timeout': 1,
        'serializer': {
            'class': "aiocache.serializers.PickleSerializer"
        },
        'plugins': [
            {'class': "aiocache.plugins.HitMissRatioPlugin"},
            {'class': "aiocache.plugins.TimingPlugin"}
        ]
    }
})


async def default_cache():
    cache = caches.get('memcached_alt')   # This always returns the SAME instance
    await cache.set("key1", {'sad':2, 'sads':[3,2]})
    print(await cache.get("key1") )
    #assert await cache.get("key") == "value"

def test_alias():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(default_cache())

    #loop.run_until_complete(caches.get('redis_alt').delete("key"))


if __name__ == "__main__":
    test_alias()