def decorato_socket(*args_decorator,**kwargs):
    def decorator_fn(fn):
        def decorator_s(*args, **kwargs):
            for i in range(args_decorator[0]):
                print(args[0])

            return fn(*args, **kwargs)
        return decorator_s
    return decorator_fn


# @decorato_socket(2)
def func(val):
    print(val)

f = decorato_socket(2)

f(func(5))


#decorato_socket(2, func(5))
#decorato_socket(2).