from werkzeug.contrib.cache import MemcachedCache
import redis

namespace = '/MedicalMessenger'

login_turbo_sms = ''
password_turbo_sms = ''

push_key_android = ''
push_key_ios = ''

COUNT_SMS = 5
START_CODE_FOR_ERROR = 404
START_CODE_FOR_GOOD = 200

MAX_LENGTH_FILE = 20971520  # 1024*1024*20

LIMIT_DIALOGS_ON_PAGE = 10
LIMIT_MESSAGES_ON_PAGE = 20
LIMIT_RESULT_SEARCH_ON_PAGE = 10
CODE_ERROR_RESPONSE = 404
format_date = '%Y-%m-%d %H:%MZ'

'''https://support.google.com/accounts/answer/6010255?hl=ru&authuser=1'''
mail_medical = 'testfortestmail13@gmail.com'
password_mail = 'testtest13'

amazon_password = 'testtest13@'
amazon_login = 'testtest13'

# roles
role_admin_app = 'admin_app'
role_partner = 'partner'
role_client = 'client'

type_work = 1




# print(type_work)
# 1 - local
# 2 - test server
# 3 - production
if type_work == 1:
    path_to_answer_api_admin = '/MedicalMessenger/api_admin/answer_server'
    path_to_answer_api_mobile = '/HeavenLife/api_mobile/answer_server'

    secret_word_for_congratulation = 'congratulation_bd'
    user = 'goods_deliver'
    database = 'medical_messenger'
    password = '86429731'
    host = 'localhost'
    port = '5430'

    str_path_to_file = '/files/'
    path_to_photo_profile = 'profile_photo/'
    path_to_photo_chat = 'chats_photo/'
    path_to_other_files = 'documents/'
    base_url = 'http://localhost:5020'
    str_connect_to_db = "dbname='medical_messenger' user='goods_deliver' host='localhost' password='86429731' port='5430'"
    domain_api = '/medical/app/api/v1'
    domain_api_mobile = '/medical/app/mobile/api/v1'
    domain_web = ''
    MEDIA_FOLDER = '/home/denis/PycharmProjects/MedicalMessenger'
    domain_photo = ''
    mail_app = ''
    password_mail_app = ''
    path_to_celery = 'redis://localhost:6379/0'
    secret_word = 'secret'
    algorithm_encode = 'HS256'
    redis_port = 26379
    rabbit_mq_port = 35672
    r_async_data = {
        "host": '127.0.0.1',
        "db": 2,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_sms_async_data = {
        "host": '127.0.0.1',
        "db": 3,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_time_async_data = {
        "host": '127.0.0.1',
        "db": 4,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_socket_async_data = {
        "host": '127.0.0.1',
        "db": 5,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_contacts_async_data = {
        "host": '127.0.0.1',
        "db": 6,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_chat_async_data = {
        "host": '127.0.0.1',
        "db": 7,
        "port": redis_port,
        "encoding": 'utf-8'
    }

    r = redis.StrictRedis(host=r_async_data['host'], port=r_async_data['port'], db=r_async_data['db'])
    r_sms = redis.StrictRedis(host=r_sms_async_data['host'], port=r_sms_async_data['port'], db=r_sms_async_data['db'])
    r_time = redis.StrictRedis(host=r_time_async_data['host'], port=r_time_async_data['port'],
                               db=r_time_async_data['db'])
    r_socket = redis.StrictRedis(host=r_socket_async_data['host'], port=r_socket_async_data['port'],
                                 db=r_socket_async_data['db'])
    r_contacts = redis.StrictRedis(host=r_contacts_async_data['host'], port=r_contacts_async_data['port'],
                                   db=r_contacts_async_data['db'])
    r_chat = redis.StrictRedis(host=r_chat_async_data['host'], port=r_chat_async_data['port'],
                               db=r_chat_async_data['db'])
    mem_data = {
        'host': 'localhost',
        'port': 22122
    }
elif type_work == 2:
    secret_word_for_congratulation = 'congratulation_bd'
    user = 'goods_deliver'
    database = 'medical_messenger'
    password = '86429731'
    host = 'localhost'
    port = '5432'

    str_path_to_file = '/files/'
    path_to_photo_profile = 'profile_photo/'
    path_to_photo_chat = 'chats_photo/'
    path_to_other_files = 'documents/'
    base_url = 'http://178.159.110.21:95'
    str_connect_to_db = "dbname='medical_messenger' user='goods_deliver' host='localhost' password='86429731' port='5432'"
    domain_api = '/medical/app/api/v1'
    domain_api_mobile = '/medical/app/mobile/api/v1'
    domain_web = ''
    MEDIA_FOLDER = '/home/den/medical_messenger'
    domain_photo = '/medical/messenger'
    mail_app = ''
    password_mail_app = ''
    path_to_celery = 'redis://localhost:6379/0'
    secret_word = 'TUyr4fnXce0ZF2vVbbOA'
    algorithm_encode = 'HS256'
    redis_port = 6379
    r_async_data = {
        "host": '127.0.0.1',
        "db": 2,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_sms_async_data = {
        "host": '127.0.0.1',
        "db": 3,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_time_async_data = {
        "host": '127.0.0.1',
        "db": 4,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_socket_async_data = {
        "host": '127.0.0.1',
        "db": 5,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_contacts_async_data = {
        "host": '127.0.0.1',
        "db": 6,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    r_chat_async_data = {
        "host": '127.0.0.1',
        "db": 7,
        "port": redis_port,
        "encoding": 'utf-8'
    }
    rabbit_mq_port = 35672
    r = redis.StrictRedis(host=r_async_data['host'], port=r_async_data['port'], db=r_async_data['db'])
    r_sms = redis.StrictRedis(host=r_sms_async_data['host'], port=r_sms_async_data['port'], db=r_sms_async_data['db'])
    r_time = redis.StrictRedis(host=r_time_async_data['host'], port=r_time_async_data['port'],
                               db=r_time_async_data['db'])
    r_socket = redis.StrictRedis(host=r_socket_async_data['host'], port=r_socket_async_data['port'],
                                 db=r_socket_async_data['db'])
    r_contacts = redis.StrictRedis(host=r_contacts_async_data['host'], port=r_contacts_async_data['port'],
                                   db=r_contacts_async_data['db'])
    r_chat = redis.StrictRedis(host=r_chat_async_data['host'], port=r_chat_async_data['port'],
                               db=r_chat_async_data['db'])
    mem_data = {
        'host': 'localhost',
        'port': 22122
    }

else:
    secret_word_for_congratulation = 'congratulation_bd'
    str_path_to_file = '/files/'
    path_to_photo_profile = 'profile_photo/'
    path_to_photo_chat = 'chats_photo/'
    base_url = ''
    str_connect_to_db = "dbname='delivery_03' user='goods_deliver' host='localhost' password='86429731' port='5430'"
    domain_api = ''
    domain_api_mobile = '/coffee/app/mobile/api/v1'
    domain_api_mobile_worker = '/coffee/app/mobile/api/v1'
    domain_web = ''
    MEDIA_FOLDER = ''
    domain_photo = ''
    mail_app = ''
    password_mail_app = ''
    path_to_celery = ''
    secret_word = 'secret'
    algorithm_encode = 'HS256'

path_to_default_photo_profile = domain_photo + str_path_to_file + path_to_photo_profile + '_default-inc.png'
path_to_default_photo_chat = domain_photo + str_path_to_file + path_to_photo_chat + 'default_.jpeg'

api_congratulation_user = 'http://localhost:4996' + domain_api_mobile + '/congratulation/user'
text_congratulation_user = '{}, с др крч!'

path_to_photo_bot_admin = domain_api_mobile + str_path_to_file + path_to_photo_profile + '_adminbot.jpg'
name_bot_admin = 'BotAdmin'
name_chat_admin = 'admin chat'
