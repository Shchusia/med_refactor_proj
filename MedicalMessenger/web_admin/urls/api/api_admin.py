from MedicalMessenger.config_app import domain_api, \
    base_url
api_get_data_admin = base_url + domain_api + '/get_data_admin'
api_update_data_admin = base_url + domain_api + '/update/data/admin'
api_upload_photo = base_url + domain_api + '/upload_file'
