from MedicalMessenger.config_app import domain_api,\
    base_url

api_get_list_partners = base_url + domain_api + '/partners'
api_create_partner = base_url + domain_api + '/partner'
api_update_partner = base_url + domain_api + '/partner/{}'  # post
api_get_partner = base_url + domain_api + '/partner/<id_partner>'  # get
