from MedicalMessenger.config_app import domain_api,\
    base_url

api_check_valid_token = base_url + domain_api + '/check_valid_token'
api_sign_in = base_url + domain_api + '/login'
api_sign_out = base_url + domain_api + '/logout'

api_restore_password = base_url + domain_api + '/restore_password'


