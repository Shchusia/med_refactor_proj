from MedicalMessenger.config_app import domain_web

url_partners = domain_web + '/partners'
url_partner = domain_web + '/partner'

url_options_partner = domain_web + '/functions/partner'
