
from flask import render_template,\
    request

from MedicalMessenger.web_admin.urls.web.web_partners import url_partner
from MedicalMessenger.web_admin.urls.api.api_partners import api_create_partner,\
    api_get_list_partners,\
    api_update_partner
import requests
from MedicalMessenger.web_admin.answer_web_server import status_code_not_200


def partners():
    res = requests.get(api_get_list_partners, headers={'Token': request.cookies.get('token')})
    if 200 <= res.status_code < 400:
        res = res.json()
        return render_template('admin/partners.html',
                               partners=res['data']['partners'],
                               url_update_partner=url_partner)

    if 404 < res.status_code < 500:
        res = res.json()
        message = res['message']['body']
    else:
        message = status_code_not_200
    return render_template('admin/partners.html', message=message)


def create_partner():
    def to_num_phone(phone_):
        answer = ''
        for i in phone_:
            if i.isnumeric():
                answer += i
        return answer

    headers = {'Token': request.cookies.get('token')}
    di = dict(request.form)
    phone = to_num_phone(di['phone'][0])
    password = di['password'][0]
    full_name = di['full_name'][0]
    email = di['email'][0]
    data = {
        'phone': phone,
        'full_name': full_name,
        'password': password
    }
    if email:
        data['email'] = email
    # print(data)
    res = requests.post(api_create_partner,
                        headers=headers,
                        json=data)
    if res.status_code == 200:
        message = res.json()['message']['body']
        res = requests.get(api_get_list_partners, headers={'Token': request.cookies.get('token')})
        res = res.json()
        return render_template('admin/partners.html',
                               partners=res['data']['partners'],
                               message=message)
    elif res.status_code == 409:
        message = res.json()['message']['body']
        res = requests.get(api_get_list_partners, headers={'Token': request.cookies.get('token')})
        res = res.json()
        return render_template('admin/partners.html',
                               partners=res['data']['partners'],
                               message=message,
                               email=email,
                               full_name=full_name)
    elif res.status_code == 411:
        message = res.json()['message']['body']
        res_ = requests.get(api_get_list_partners, headers={'Token': request.cookies.get('token')})
        res_ = res_.json()
        return render_template('admin/partners.html',
                               partners=res_['data']['partners'],
                               message=message,
                               full_name=full_name)
    res = res.json()
    return render_template('admin/partners.html',
                           partners=res['data']['partners'],
                           message=status_code_not_200)


def update_partner(id_partner):
    res = requests.get(api_get_list_partners, headers={'Token': request.cookies.get('token')})
    res = res.json()
    partners_ = res['data']['partners']
    partner = None
    for p in partners_:
        if str(p['id_partner']) == id_partner:
            partner = p
            break

    if request.method == 'POST':
        di = dict(request.form)
        # print(di)
        data = {}
        email = di['email'][0]
        password = di['password'][0]
        full_name = di['full_name'][0]
        if email and not email.isspace():
            data['email'] = email
        if password and not password.isspace():
            data['password'] = password
        if full_name and not full_name.isspace():
            data['full_name'] = full_name
        res = requests.post(api_update_partner.format(id_partner),
                            headers={'Token': request.cookies.get('token')},
                            json=data)
        res_ = requests.get(api_get_list_partners, headers={'Token': request.cookies.get('token')})
        res_ = res_.json()
        partners_ = res_['data']['partners']
        if res.status_code == 200:
            return render_template('admin/partners.html',
                                   partners=partners_,
                                   message=res.json()['message']['body'],
                                   url_update_partner=url_partner)
        elif res.status_code == 404:
            return render_template('admin/partners.html',
                                   partners=partners_,
                                   message=status_code_not_200,
                                   url_update_partner=url_partner)
        else:
            return render_template('admin/partners.html',
                                   partners=partners_,
                                   partner=partner,
                                   message=res.json()['message']['body'],
                                   url_update_partner=url_partner)

    return render_template('admin/partners.html',
                           partners=partners_,
                           partner=partner,
                           url_update_partner=url_partner)


