
from flask import render_template,\
    jsonify,\
    request,\
    redirect, \
    url_for,\
    make_response
from MedicalMessenger.web_admin.urls.api.api_admin import api_get_data_admin,\
    api_update_data_admin,\
    api_upload_photo

import requests
from MedicalMessenger.web_admin.answer_web_server import status_code_not_200


def update_admin():
    message = ''
    if request.method == 'POST':
        di = dict(request.form)
        # print(di)
        path_to_file = ''
        file = request.files.getlist("file")[0]
        if file.content_type == 'image/jpeg':
            files = {'file': request.files.getlist("file")[0]}
            res = requests.post(api_upload_photo,
                                files=files,
                                headers={'Token': request.cookies.get('token')})
            if res.status_code == 200 and res.json()['message']['code'] > 0:
                path_to_file = res.json()['data']['path_to_file']
        full_name = di['full_name'][0]
        id_city = di['city'][0]
        email = di['email'][0]
        password = di["password"][0]
        data = {
            'email': email,
            'id_city': id_city,
            'full_name': full_name
        }
        if password and not password.isspace():
            data['password'] = password
        if path_to_file:
            data['path_to_file'] = path_to_file
        res = requests.post(api_update_data_admin,
                            headers={
                                'Token': request.cookies.get('token')
                            },
                            json=data)
        if res.status_code == 200:
            token = res.headers['Token']
            resp = make_response(redirect(url_for('update_admin')))
            resp.set_cookie('token', token)
            return resp

        elif 404 <= res.status_code < 500:
            message = res.json()['message']['body']
        else:
            message = status_code_not_200

    res = requests.get(api_get_data_admin,
                       headers={
                           'Token': request.cookies.get('token')
                       })
    if 200 <= res.status_code <= 400:
        res = res.json()
        return render_template('admin/admin_options.html',
                               admin=res['data']['data_admin'],
                               cities=res['data']['cities'],
                               message=message)
    else:
        return render_template('admin/admin_options.html',
                               message=message + res.json()['message']['body'])



