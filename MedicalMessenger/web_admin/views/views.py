from MedicalMessenger.web_admin.urls.web.web_admin import url_update_admin

from MedicalMessenger.web_admin.urls.web.web_partners import url_options_partner

from MedicalMessenger.web_admin.urls.web.web_partners import url_partners
from MedicalMessenger.web_admin.urls.web.web_entry import url_index
from MedicalMessenger.web_admin.urls.web.web_entry import url_sign_in
from MedicalMessenger.web_admin.urls.web.web_entry import url_sign_out
from MedicalMessenger.web_admin.urls.web.web_entry import url_restore_password
from MedicalMessenger.web_admin.urls.api.api_entry import api_check_valid_token
from MedicalMessenger.web_admin.urls.api.api_entry import api_sign_in
from MedicalMessenger.web_admin.urls.api.api_entry import api_sign_out
from MedicalMessenger.web_admin.urls.api.api_entry import api_restore_password
from MedicalMessenger.config_app import role_partner
from MedicalMessenger.config_app import role_admin_app
import hashlib
from MedicalMessenger.web_admin.answer_web_server import unknown_role

from MedicalMessenger.web_admin.views.views_admin.view_partners import *
from MedicalMessenger.web_admin.views.views_admin.view_admin import *
from MedicalMessenger.web_admin.views.views_partner.view_functions_partner import *


def index():
    token = request.cookies.get('token')
    if token:
        res = requests.get(api_check_valid_token, headers={'Token': token})
        # print(res.status_code)
        if 200 <= res.status_code < 404:
            res = res.json()
            if res['message']['code'] > 0:  # значит валидны
                role = request.cookies.get('role')
                if role == role_admin_app:
                    # админ приложения
                    return redirect(url_for('partners'))
                elif role == role_partner:
                    # админ сети какой - то
                    # return make_response(redirect(url_for('shops')))
                    return redirect(url_for('partner_functions'))

        return render_template('except.html')
    return redirect(url_for('sign_in'))


def sign_in():
    def create_hash_password(password):
        return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()

    message = ''
    if request.method == 'POST':
        di = dict(request.form)
        data = {
            'email': di['login'][0],
            'password_hash': create_hash_password(di['password'][0])
        }
        print(api_sign_in)
        res = requests.post(api_sign_in,
                            json=data)
        if 200 < res.status_code < 400:
            for_token = res
            res = res.json()
            if res['message']['code'] > 0:
                token = for_token.headers['Token']
                # print(res['data'])
                role = for_token.headers['Role']
                if role == role_admin_app:
                    resp = make_response(redirect(url_for('partners')))
                elif role == role_partner:
                    resp = make_response(redirect(url_for('partner_functions')))
                else:
                    return render_template('sign_in.html',
                                           message=unknown_role)
                resp.set_cookie('token', token)
                resp.set_cookie('role', role)

                return resp

            message += res['message']['body']
        else:
            if res.status_code > 404:
                res = res.json()
                message += res['message']['body']
            else:
                message += status_code_not_200

    return render_template('sign_in.html',
                           message=message)


def sign_out():
    message = ''
    token = request.cookies.get('token')
    requests.get(api_sign_out, headers={'Token': token})
    resp = make_response(render_template('sign_in.html',
                                         message=message))
    resp.set_cookie('token', token)
    resp.set_cookie('id', str(0))
    resp.set_cookie('role', '')
    return resp


def restore():
    di = dict(request.form)
    email = di['email'][0]
    res = requests.post(api_restore_password, json={'email':email})
    if res.status_code >= 200 and  res.status_code < 404:
        res = res.json()
        message = res['message']['body']
    else:
        if res.status_code == 404:
            message = status_code_not_200
        else:
            res = res.json()
            message = res['message']['body']
    return render_template('sign_in.html',
                           message=message)


routes = [
    (url_index, 'index', index, ['POST', 'GET']),
    (url_sign_in, 'sign_in', sign_in, ['POST', 'GET']),
    (url_sign_out, 'logout', sign_out, ['GET']),
    (url_restore_password, 'restore', restore, ['POST']),
    (url_options_partner, 'partner_functions', partner_functions, ['GET']),
    (url_update_admin, 'update_admin', update_admin, ['POST', 'GET']),
    (url_partners, 'partners', partners, ['GET']),
    (url_partner, 'partner', create_partner, ['POST']),
    (url_partner + '/<id_partner>', 'update_partner', update_partner, ['POST', 'GET']),
]
