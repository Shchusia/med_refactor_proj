from flask import Flask,\
    render_template
app = Flask(__name__)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


def init_routes(app_r):
    from MedicalMessenger.web_admin.views.views import routes
    for route in routes:
        app_r.add_url_rule(route[0],
                           route[1],
                           route[2],
                           methods=route[3])


init_routes(app)
