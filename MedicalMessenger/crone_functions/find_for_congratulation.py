from MedicalMessenger.config_app import user as us_db,\
    host,\
    database,\
    password,\
    port,\
    api_congratulation_user,\
    secret_word_for_congratulation
import asyncpg
import requests
import psycopg2
from MedicalMessenger.config_app import str_connect_to_db
import traceback
from MedicalMessenger.api_admin.server_answers import error_in_select,\
    error_in_connect


def create_connection():
    """
    метод создания подключения к БД
    :return: или подключение или None
    """
    conn = None
    try:
        conn = psycopg2.connect(str_connect_to_db)
    except:
        print("I am unable to connect to the database")
    return conn


def find_users():
    conn = create_connection()
    if conn:
        cur = conn.cursor()
        select = '''
        SELECT u.id_user, u.full_name, u.phone
        FROM users u
        WHERE extract(DAY FROM u.birth_day) = extract(DAY FROM now())
        AND extract(MONTH FROM u.birth_day) = extract(MONTH FROM now())
        '''
        cur.execute(select)

        list_c = cur.fetchall()
        conn.close()

        # print(api_congratulation_user)
        for l in list_c:
            # print(l)
            res = requests.post(api_congratulation_user,
                                headers={'Secret-Word': secret_word_for_congratulation},
                                data={'id_user': l[0],
                                      'full_name': l[1],
                                      'phone': l[2]})

if __name__ == '__main__':
    find_users()
