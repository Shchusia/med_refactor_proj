from datetime import datetime
from MedicalMessenger.config_app import format_date


def get_date(date_str):
    try:
        return datetime.strptime(date_str, format_date)
    except ValueError:
        return None
    except:
        return None

