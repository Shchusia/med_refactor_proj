import jwt
import random
import hashlib
import traceback
from MedicalMessenger.config_app import secret_word
from MedicalMessenger.config_app import algorithm_encode


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def generate_code(length_code=6):
    return ''.join([str(random.randint(0, 9)) for _ in range(0, length_code)])


def create_hash_password(password):
    return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()


def phone_to_easy_phone(phone_):
        '''+995322ХХХХХХ
        +380979491074
        +79215789560
        '''
        answer = ''
        for p in phone_:
            if p.isnumeric():
                answer += p
        return answer


def user_to_token(user):
    '''
    преаброзует данные пользователя в токен
    :param user:
    :return:
    '''
    tmp = {
        'id_client': user['id_client'],
        'is_sponsor': user['is_sponsor']
    }
    return data_to_token(tmp)
