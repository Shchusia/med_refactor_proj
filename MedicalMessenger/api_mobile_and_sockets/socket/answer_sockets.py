good_send_message = {
    'title': 'успех',
    'body': 'вы успешно отправили ообщение',
    'code': 9,
}

good_remove = {
    'title': 'успех',
    'body': 'вы успешно удалили сообщения',
    'code': 10
}


# !__ errors __! #

error_permission = {
    'title': 'ошибка',
    'body': 'вы не можете слать сообщения в данный чат',
    'code': -21,
}


arguments_error = {
    'title': 'ошибка',
    'body': 'нет необходимых аргументов для выполнения запроса',
    'code': -30
}

not_valid_token = {
    'title': 'ошибка токена',
    'body': 'не валидный токен',
    'code': -31,
}

error_unknown = {
    'title': 'ошибка',
    'body': 'неизвестная ошибка',
    'code': -32
}

error_in_select = {
    'title': 'ошибка в запросе',
    'body': 'ошибка в запросе к базе данных',
    'code': -33
}

error_in_connect = {
    'title': 'ошибка при коннекте',
    'body': 'ошибка попытке подключиться к базе данных',
    'code': -34
}

good_select = {
    'title': 'успех',
    'body': 'запрос успешно выполнен',
    'code': 6,
}
