from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_data import add_socket, \
    remove_socket, \
    get_sid_user, \
    disconnect_


from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_contacts import add_contacts, \
    get_contacts
from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_data import set_last_time_user_online,get_last_entry_time
from MedicalMessenger.api_mobile_and_sockets.answer_server import unknown_error,\
    error_arguments
import traceback
from MedicalMessenger.api_mobile_and_sockets.urls.socket import socket_my_connect,\
    socket_my_disconnect,\
    socket_error, \
    socket_online_status,\
    socket_typing
import time
from MedicalMessenger.api_mobile_and_sockets.socket.message.db_socket_message import get_contacts_user
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data
from MedicalMessenger.config_app import namespace
from MedicalMessenger.api_mobile_and_sockets.__init__ import sio


@sio.on(socket_my_connect, namespace=namespace)
async def connect(sid, message):
    print('my conn')
    try:
        token = message['token']

        is_good, data = token_to_data(token)

        await add_socket(token,
                         data['id_user'],
                         sid,
                         sio.redis_socket)

        res = await get_sid_user(data['id_user'],
                                 sid,
                                 r=sio.redis_socket)
        await set_last_time_user_online(data['id_user'],
                                        0,
                                        sio.redis_socket)
        if len(res) == 0:
            contacts = await get_contacts(data['id_user'],
                                          sio.redis_socket)  # все контакты пользователя
            if len(contacts) == 0:
                contacts = await get_contacts_user(message['id_user'])
                if contacts['message']['code'] > 0:
                    for c in contacts['data']['contacts']:
                        await add_contacts(message['id_user'],
                                           c,
                                           sio.redis_socket)
                else:
                    await sio.emit(socket_error,
                                   {'data': contacts['message']},
                                   room=sid,
                                   namespace=namespace)
            for con in contacts:
                list_sids = await get_sid_user(con,
                                               r=sio.redis_socket)
                for l in list_sids:
                    await sio.emit(socket_online_status,
                                   {'data': {
                                       'id_user': message['id_user'],
                                       'is_online': True
                                   }},
                                   room=l,
                                   namespace=namespace)

    except KeyError:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        await sio.emit(socket_error,
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on(socket_my_disconnect, namespace=namespace)
async def disconnect(sid, message):
    try:
        token = message['token']
        is_good, data = token_to_data(token)
        if message.get('sid', False):
            await remove_socket(token,
                                data['id_user'],
                                message['sid'],
                                sio.redis_socket)
        else:
            await remove_socket(token,
                                data['id_user'],
                                sid,
                                sio.redis_socket)
    except KeyError:
        await sio.emit(socket_error,
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        await sio.emit(socket_error,
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on('connect', namespace=namespace)
async def test_connect(sid, environ):
    print('connect user')
    await sio.emit('my response',
                   {'data': 'Connected', 'count': 0},
                   room=sid,
                   namespace=namespace)


@sio.on('reconnect', namespace=namespace)
async def reconnect(sid, environ):
    print('reconnect')
    # print(environ)


@sio.on('disconnect', namespace=namespace)
async def disconnect(sid):
    print('Client disconnected')
    id_user = await disconnect_(sid,
                                sio.redis_socket)
    sids_user = await get_sid_user(id_user,
                                   r=sio.redis_socket)
    contacts = await get_contacts(id_user,
                                  sio.redis_socket)  # все контакты пользователя
    if len(contacts) == 0:
        contacts = await get_contacts_user(id_user)
        if contacts['message']['code'] > 0:
            for c in contacts['data']['contacts']:
                await add_contacts(id_user,
                                   c,
                                   sio.redis_socket)
        else:
            await sio.emit(socket_error,
                           {'data': contacts['message']},
                           room=sid,
                           namespace=namespace)
    is_emit_offline = False
    if len(sids_user) == 0:
        is_emit_offline = True
        await set_last_time_user_online(id_user,
                                        time.time(),
                                        sio.redis_socket)
    for con in contacts:
        list_sids = await get_sid_user(con,
                                       r=sio.redis_socket)
        for l in list_sids:
            await sio.emit(socket_typing,
                           {'data': {
                               'id_chat': 0,
                               'id_user': id_user,
                               'is_typing': False
                           }},
                           room=l,
                           namespace=namespace)
            if is_emit_offline:
                await sio.emit(socket_online_status,
                               {'data': {
                                   'id_user': id_user,
                                   'is_online': False
                               }
                               },
                               room=l,
                               namespace=namespace)


import MedicalMessenger.api_mobile_and_sockets.socket.message.socket_message
