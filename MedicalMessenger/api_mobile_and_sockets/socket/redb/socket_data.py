# from config_app import r_socket as r


async def add_socket(session_key, id_user, sid, r):
    # r.set('{}_{}'.format(session_key, id_user), sid)
    await r.lpush('{}'.format(id_user), sid)
    await r.set(sid, id_user)


async def remove_socket(session_key, id_user, sid, r):
        try:
            await r.lrem('{}'.format(id_user), 1, sid)
            # r.delete('{}_{}'.format(session_key, id_user))
        except:
            pass


async def disconnect_(sid, r):
    try:
        id_us = await r.get(sid)
        await r.lrem('{}'.format(id_us), 1, sid)
        await r.delete(sid)
        return id_us
    except:
        return 0


async def get_sid_user(id_user, sid=None, r=None):
    l = list(await r.lrange('{}'.format(id_user), 0, -1))
    res = []
    if sid:
        for li in l:
            if li != sid:
                res.append(li)
    else:
        for li in l:
            res.append(li)
    return res


async def get_last_entry_time(id_user, r):
    try:
        last_online_time = int(await r.get('last_online_{}'.format(id_user)))
        return last_online_time
    except:
        return -1


async def set_last_time_user_online(id_user,
                                    time,
                                    r):
    await r.set('last_online_{}'.format(id_user),
                time)
