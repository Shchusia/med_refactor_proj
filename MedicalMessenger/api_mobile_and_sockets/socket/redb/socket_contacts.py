# from config_app import r_contacts as r_c,\
#     r_socket as r


async def add_contacts(id_user, him_contacts, r_c):
    await r_c.lpush(id_user, him_contacts)


async def rem_contacts(id_user, who_delete, r):
    try:
        await r.lrem(id_user, 1, who_delete)
    except:
        pass


async def get_contacts(id_user, r):
    try:
        l = list(set(await r.lrange(id_user, 0, -1)))
        return l
    except:
        return []
