

async def set_last_sender_chat(id_chat, id_sender, r_u):
    await r_u.set('chat_sender_{}'.format(id_chat), id_sender)


async def get_last_sender_chat(id_chat, r_u):
    try:
        return int(await r_u.get('chat_sender_{}'.format(id_chat)))
    except:
        return None


async def set_event_user_from_chat(id_user,
                                   id_chat,
                                   count_event,
                                   r_u):
    await r_u.set('event_{}_chat_{}'.format(id_user,
                                            id_chat),
                  count_event)


async def add_event_user(id_user,
                         id_chat,
                         r_u):
    try:
        events = int(await r_u.get('event_{}_chat_{}'.format(id_user,id_chat)))
    except:
        events = 0
    events += 1
    await set_event_user_from_chat(id_user,
                                   id_chat,
                                   events,
                                   r_u)


async def get_events_user_chat(id_user,
                               id_chat,
                               r_u):
    try:
        return int(await r_u.get('event_{}_chat_{}'.format(id_user,
                                                           id_chat)))
    except:
        return None


