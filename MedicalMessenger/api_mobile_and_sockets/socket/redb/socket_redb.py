import traceback
# from config_app import r


async def check_valid_data_multi(id_user, session_key, role, r):
    try:
        for li in list(await r.lrange('{}_{}'.format(role,id_user), 0, -1)):
            if li == session_key:
                return True
        return False
    except:
        traceback.print_exc()
    return False
