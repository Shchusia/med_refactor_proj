
async def get_contacts_user_mem(id_user,cache):
    try:
        return await cache.get('contacts_user_{}'.format(id_user))
    except:
        return None


async def set_contacts_user_mem(id_user, contacts, cache):
    await cache.set('contacts_user_{}'.format(id_user), contacts)


async def remove_contacts_user_men(id_user, cache):
    await cache.set('contacts_user_{}'.format(id_user), None)


async def set_list_chat_to_user(id_user, list_, cache):
    await cache.set('chats_user_{}'.format(id_user), list_)


async def get_chats_user(id_user, cache):
    return await cache.get('chats_user_{}'.format(id_user))


async def add_new_chat_to_user(id_user, id_chat, cache):
    chats = await get_chats_user(id_user,cache)
    if chats:
        chats.append(id_chat)
    else:
        chats = [id_chat]
    await set_list_chat_to_user(id_user, list(set(chats)), cache)


async def get_users_chat(id_chat,cache):

    return await cache.get('users_on_chat_{}'.format(id_chat))


async def set_users_to_chat(id_chat, list_users, cache):
    await cache.set('users_on_chat_{}'.format(id_chat), list_users)


async def remove_users_from_chat(id_chat, users, cache):
    for user in users:
        await remove_user_from_chat(id_chat, user)


async def add_user_to_chat(id_chat, id_user, cache):
    users = await get_users_chat(id_chat, cache)
    if users:
        list(set(users.append(id_user)))
        await set_users_to_chat(id_chat, users, cache)
    else:
        await set_users_to_chat(id_chat, [id_user],cache)


async def remove_user_from_chat(id_chat, id_user, cache):
    users = await get_users_chat(id_chat, cache)
    if users:
        for i, val in enumerate(users):
            if int(val) == int(id_user):
                del users[i]
                break
        await set_users_to_chat(id_chat, users, cache)


if __name__ =='__main__':
    set_list_chat_to_user(1, None)
    set_users_to_chat(24,None)