# from api_mobile.postgres.postdb import decorator_connect
from MedicalMessenger.api_mobile_and_sockets.socket.memdb.mem_socket import get_chats_user,\
    set_list_chat_to_user,\
    get_users_chat,\
    set_users_to_chat,\
    get_contacts_user_mem
from MedicalMessenger.api_mobile_and_sockets.socket.answer_sockets import error_permission,\
    good_send_message, \
    good_remove,\
    good_select
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat_db import set_count_messages,\
    get_count_messages,\
    add_new_message
from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_chat import set_last_sender_chat,\
    get_last_sender_chat,\
    add_event_user,\
    get_events_user_chat,\
    set_event_user_from_chat

from MedicalMessenger.config_app import str_connect_to_db,user as us_db,host,database,password,port
import asyncpg
from MedicalMessenger.api_mobile_and_sockets.socket.memdb.mem_socket import set_contacts_user_mem

from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat_db import get_count_messages
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_user import set_information_user, \
    get_information_user
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat import set_information_chat, \
    get_information_chat, \
    set_primary_chat_partner, \
    set_type_sub_chat, \
    set_chat_user, \
    set_to_chats, \
    set_list_chat, \
    set_list_chat_to_user, \
    get_chats_user, \
    get_chats
from MedicalMessenger.api_mobile_and_sockets.answer_server import good_get_chat, \
    error_not_your_chat_for_view, \
    good_entry_to_chat, \
    error_not_entry_to_chat_ald_code, \
    error_not_entry_you_in_chat, \
    good_create_dialog, \
    error_chat_exist_with_user, \
    default_name_dialog, \
    error_not_connect_to_chat, \
    error_entry_chat_exist_in_chat, \
    error_not_create_chat_by_not_in_one_chat, \
    good_generate_code, \
    good_get_types_chat


from MedicalMessenger.config_app import role_client, role_partner, \
    path_to_default_photo_profile

async def get_data_chat_main(id_chat,
                             id_user,
                             conn,
                             cache):

    select = '''
        SELECT id_chat, title, description, path_img, code_for_join,is_main_chat, created, is_chat, ttl_code,ref_id_creater
        FROM chat, user_to_chat
        WHERE id_chat = {}
        AND id_chat = ref_id_chat
        AND ref_id_user  = {}
    '''.format(id_chat,
               id_user)
    row = await conn.fetchrow(select)
    data = {
        'id_chat': row[0],
        'title_chat': row[1],
        'description': row[2],
        'path_to_img': row[3],
        'code_for_join': row[4],
        'is_main_chat': row[5],
        'created_on': str(row[6]),
        'is_chat': row[7],
        'ttl_code': row[8],
        'creator': await db_get_user(row[9],
                                     conn,
                                     cache),
        'other_information': '',
        'users_in_chat': (await get_users_chat(row[0], conn))
    }
    return data


async def get_chat_data(id_chat,
                        id_user,
                        cache,
                        pool):
    data = await get_information_chat(id_chat,
                                      cache)
    if data is None:
        async with pool.acquire() as conn:
            select = '''
                    SELECT id_chat, title, description, path_img, code_for_join,is_main_chat, created, is_chat, ttl_code,ref_id_creater
                    FROM chat, user_to_chat
                    WHERE id_chat = {}
                    AND id_chat = ref_id_chat
                    AND ref_id_user  = {}
                '''.format(id_chat,
                           id_user)
            row = await conn.fetchrow(select)
            if row:
                data = {
                    'id_chat': row[0],
                    'title_chat': row[1],
                    'description': row[2],
                    'path_to_img': row[3],
                    'code_for_join': row[4],
                    'is_main_chat': row[5],
                    'created_on': str(row[6]),
                    'is_chat': row[7],
                    'ttl_code': row[8],
                    'creator': await db_get_user(row[9],
                                                 conn,
                                                 cache),
                    'other_information': None,
                    'users_in_chat': (await get_users_chat(row[0],
                                                           conn))
                }
                if not row[5]:
                    select = '''
                            SELECT id_chat, ref_main_chat, ref_type_sub_chat, type_chat
                            FROM chat, type_sub_chat
                            WHERE id_chat = {}
                            AND ref_type_sub_chat = type_sub_chat.id_type_sub_chat
                        '''.format(id_chat)
                    row = await conn.fetchrow(select)
                    other_data = {
                        'main_chat': await get_data_chat_main(row[0],
                                                              id_user,
                                                              conn,
                                                              cache),
                        'id_type_sub_chat': row[2],
                        'type_sub_chat': row[3]
                    }
                    data['other_information'] = other_data
                else:
                    select = '''
                        select id_chat, title, description, path_img, type_chat
                        FROM chat, type_sub_chat
                        where ref_main_chat  = {}
                        AND ref_type_sub_chat = type_sub_chat.id_type_sub_chat
                        ORDER BY created DESC
                    '''.format(id_chat)
                    sub_chats = [
                        {
                            'id_chat': r[0],
                            'title_chat': r[1],
                            'description': r[2],
                            'path_to_img': r[3],
                            'type_sub_cha': r[4]

                        } for r in await  conn.fetch(select)
                    ]
                    data['sub_chats'] = sub_chats
                await set_information_chat(id_chat,
                                           data,
                                           cache)

                return {
                    'code': 1,
                    'message': good_get_chat,
                    'data': {
                        'chat_info': data
                    }
                }
            return {
                'code': 1,
                'message': error_not_your_chat_for_view,
                'data': {}
            }
    return {
        'code': 1,
        'message': good_get_chat,
        'data': {
            'chat_info': data
        }
    }



async def db_get_user(id_user,
                      conn,
                      cache):
    '''
    получение данных пользователя
    :param id_user: айди пользователя
    :param conn: объект коннекта к базе
    :param cache: объект memcache
    :return: пользователь
    '''

    user = await get_information_user(id_user,
                                      cache)
    if user is None:
        select = '''
            SELECT u.id_user, u.phone, u.path_img, u.full_name, u.ref_id_role
            FROM users u
            WHERE u.id_user = {}
        '''.format(id_user)

        row = await conn.fetchrow(select)
        user = {
            'id_user': row[0],
            'phone': row[1],
            'full_name': row[3],

        }
        select = '''
            SELECT  c.city_title, r.region_
            FROM users u, city c, region r
            WHERE u.id_user = {}
            AND u.ref_id_city = c.id_city
            AND c.ref_id_region = r.id_region
        '''.format(row[0])
        row_c = await conn.fetchrow(select)
        if row_c:
            user['city_title'] = row_c[0]
            user['region'] = row_c[1]
        else:
            user['city_title'] = ''
            user['region'] = ''

        if row[4] == 3:
            user['role'] = role_client
            select = '''
                SELECT specialisation_
                FROM users, specialisation
                WHERE ref_id_specialisation = specialisation.id_specialisation
                AND id_user = {}
            '''.format(id_user)

            try:
                user['specialisation'] = (await conn.fetchrow(select))[0]
            except:
                user['specialisation'] = 0
        else:
            if row[4] == 2:
                user['role'] = role_partner
            else:
                user['role'] = role_partner
            user['specialisation'] = role_partner
        if row[2]:
            user['path_img'] = row[2]
        else:
            user['path_img'] = path_to_default_photo_profile
        await set_information_user(id_user,
                                   user,
                                   cache)
    return user


async def is_exist_user_in_chat(id_chat,
                                id_user,
                                conn,
                                cache):
    data = await get_chats_user(id_user,
                                cache)
    if not data:
        select = '''
            SELECT ref_id_chat
            FROM user_to_chat
            WHERE ref_id_user = {} 
        '''.format(id_user)
        data = [str(r[0]) for r in await conn.fetch(select)]
        await set_list_chat_to_user(id_user,
                                    data,
                                    cache)
    for d in data:
        if int(d) == int(id_chat):
            return True
    return False


async def participant_in_dialogue(id_chat,
                                  id_user,
                                  conn=None,
                                  cache=None,
                                  pool=None):

    select = '''
        SELECT ref_id_user
        FROM user_to_chat
        WHERE ref_id_chat = {}
        AND is_ban = FALSE 
    '''.format(id_chat)
    if conn:
        res = await conn.fetch(select)
        users = [str(r[0]) for r in res]
        await set_users_to_chat(id_chat,
                                users,
                                cache)
        return users
    else:
        async with pool.acquire() as conn:
            users = [str(r[0]) for r in await conn.fetch(select)]
        await set_users_to_chat(id_chat,
                                users,
                                cache)
        return users


async def send_message_db(id_user,
                          text_message,
                          id_chat,
                          type_message,
                          pool,
                          cache,
                          r_chat):
    async with pool.acquire() as conn:
        dat = await is_exist_user_in_chat(id_chat,
                                          id_user,
                                          conn,
                                          cache)
        if dat:
            participants = await participant_in_dialogue(id_chat,
                                                         id_user,
                                                         conn)
            if len(set(participants) & {str(id_user)}) > 0:
                insert_message = '''
                    INSERT INTO messages (id_message, ref_id_message_type, ref_id_sender, ref_id_chat, created_on, text_message) 
                    VALUES (DEFAULT, {},{},{}, DEFAULT, '{}') RETURNING created_on, id_message
                '''.format(type_message, id_user, id_chat, text_message.replace("'", "''"))
                row = await conn.fetchrow(insert_message)
                # cur.execute(insert_message)
                # conn.commit()
                # row = cur.fetchone()

                select_message_to_user = '''
                    select send_message_to_user(ARRAY[{}], {}, {}, {})
                '''.format(','.join(participants),
                           id_chat,
                           text_message,
                           id_user)
                await conn.execute(select_message_to_user)
                for p in participants:
                    if p != id_user:
                        await add_event_user(p, id_chat, r_chat)
                # conn.commit()
                # set_last_sender_chat(id_chat, id_user)  # для реализации просмотра сообщений
                user = await db_get_user(id_user, conn, cache)
                chat = (await get_chat_data(id_chat,
                                            id_user,
                                            cache,
                                            pool))['data']['chat_info']
                # print(chat)
                data = {
                    'id_message': row[1],
                    'text_message': text_message,
                    'created_on': str(row[0]),
                    'ref_id_sender': id_user,
                    'name_sender': user['full_name'],
                    'id_type_message': type_message,
                    'id_chat': id_chat,
                    'title_chat': chat['title_chat'],
                    'is_chat': chat['is_chat'],
                    'path_img': user['path_img'],
                    'count_new': (await get_count_messages(id_user, id_chat, cache))
                }
                await conn.close()
                return {
                    'code': 1,
                    'message': good_send_message,
                    'data': {
                        'participants': participants,
                        'message': data
                    }
                }
        await conn.close()
        return {
            'code': 0,
            'message': error_permission
        }


async def remove_message(id_user,
                         messages,
                         is_for_all,
                         id_chat,
                         pool,
                         cache):
    for_all = str(is_for_all).lower()
    for_all = True if for_all == 'true' else False
    async with pool.acquire() as conn:
        select = '''
            select remove_message_user(ARRAY[{}], {}, {}, {})
        '''.format(','.join(messages), id_chat, id_user, for_all)
        row = await conn.fetchrow(select)

        if row[0] == 1:
            if for_all:
                participants = await participant_in_dialogue(id_chat, id_user, conn, cache)
            else:
                participants = [id_user]

            return {
                'code': 1,
                'message': good_remove,
                'data': {
                    'participants': participants,
                    'message': messages
                }
            }

        return {
            'code': 0,
            'message': error_permission
        }


async def get_id_lsat_sender_chat(id_chat,
                                  conn,
                                  r_chat):
    id_last_sender = await get_last_sender_chat(id_chat, r_chat)
    if id_last_sender:
        return id_last_sender
    select = '''
        SELECT ref_id_user
        FROM chat_message, chat
        WHERE id_chat_message = (
                                  SELECT max(id_chat_message)
                                  FROM chat_message
                                  WHERE ref_id_chat = {})
        AND id_chat = ref_id_chat
    '''.format(id_chat)

    try:
        row = await conn.fetchrow(select)
        if row[0]:
            await set_last_sender_chat(id_chat,
                                       row[0],
                                       r_chat)
            return row[0]
        return 0
    except:
        # traceback.print_exc()
        return 0


async def entry_to_chat(id_chat,
                        id_user,
                        pool,
                        cache,
                        r_chat):
    async with pool.acquire() as conn:
        if await is_exist_user_in_chat(id_chat,
                                       id_user,
                                       conn,
                                       cache):
            select = '''
                DELETE FROM chat_event_user WHERE ref_id_chat = {} AND ref_id_user = {}
            ''' .format(id_chat, id_user)
            await conn.execute(select)
            await set_event_user_from_chat(id_user,
                                           id_chat,
                                           0,
                                           r_chat)
            return 0
    return 0


async def get_contacts_user(id_user, pool, cache):

    data = await get_contacts_user_mem(id_user,
                                       cache)
    if data:
        pass
    else:
        select = '''
            SELECT DISTINCT ct.ref_id_user
            FROM user_to_chat ct
            WHERE ct.ref_id_chat in (
              SELECT u.ref_id_chat
              FROM user_to_chat u
              WHERE u.ref_id_user = {}
            )
            AND ct.ref_id_user != {}
        '''.format(id_user, id_user)
        async with pool.acquire() as conn:
            data = [r[0] for r in await conn.fetch(select)]
        await set_contacts_user_mem(id_user, data, cache)
    return {
        'code': 1,
        'message': good_select,
        'data': {
            'contacts': data
        }
    }


if __name__ == '__main__':
    remove_message(15, [24, 25], True, 30)

