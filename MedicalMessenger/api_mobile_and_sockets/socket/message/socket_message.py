from MedicalMessenger.api_mobile_and_sockets.__init__ import sio
from MedicalMessenger.config_app import namespace
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data
from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_redb import check_valid_data_multi
import traceback
from MedicalMessenger.api_mobile_and_sockets.socket.message.db_socket_message import send_message_db,\
    remove_message,\
    participant_in_dialogue,\
    entry_to_chat
from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_data import add_socket, \
    remove_socket, \
    get_sid_user, \
    disconnect_
from MedicalMessenger.api_mobile_and_sockets.celery_med.celery_send_push import send_push_message
from MedicalMessenger.api_mobile_and_sockets.answer_server import error_arguments,\
    unknown_error,\
    not_valid_token,\
    error_not_valid_phone,\
    error_valid_data
from MedicalMessenger.api_mobile_and_sockets.urls.socket import socket_error,\
    socket_new_message,\
    socket_send_message,\
    socket_remove_message,\
    socket_removed_messages,\
    socket_typing,\
    socket_viewed_dialog,\
    socket_you_typing,\
    socket_entry_to_chat


@sio.on(socket_send_message, namespace=namespace)
async def send_message(sid, message):
    try:

        token = message['token']
        is_good, data = token_to_data(token)
        r = await check_valid_data_multi(data['id_user'],
                                         token,
                                         data['role'],
                                         sio.redis_)
        if r:
            chat_id = message['message']['id_chat']
            message_text = message['message']['text_message']
            try:
                type_message = message['message']['type_message']
            except KeyError:
                type_message = 1

            data = await send_message_db(data['id_user'],
                                         message_text,
                                         chat_id,
                                         type_message,
                                         sio.pool,
                                         sio.mem,
                                         sio.redis_chat)

            if data['message']['code'] >= 1:
                # print(data['data']['participants'])
                for lp in data['data']['participants']:

                    sid_pa = await get_sid_user(lp,
                                                r=sio.redis_socket)
                    # print(sid_pa)
                    if sid_pa:
                        for s in sid_pa:
                            await sio.emit(socket_new_message,
                                           {'data': data['data']['message']},
                                           room=s,
                                           namespace=namespace)
                    else:
                        try:
                            # отправка пуша
                            send_push_message.delay(lp, data['data']['message'])
                        except:
                            traceback.print_exc()
                        pass

            else:
                await sio.emit(socket_error,
                               {'data': data['message']},
                               room=sid,
                               namespace=namespace)
        else:
            await sio.emit(socket_error,
                           {'data': not_valid_token},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on(socket_remove_message, namespace=namespace)
async def remove_messages(sid, message):
    '''
    :param sid:
    :param message:
    :return:
    '''
    try:

        token = message['token']
        is_good, data = token_to_data(token)
        r = await check_valid_data_multi(data['id_user'],
                                         token,
                                         data['role'],
                                         sio.redis_)
        if r:
            messages_for_remove = message['messages_for_remove']
            id_chat = message['id_chat']
            for_all = False
            try:
                for_all = message['for_all']
            except KeyError:
                for_all = False
            res = await remove_message(data['id_user'],
                                       messages_for_remove,
                                       for_all,
                                       id_chat,
                                       sio.pool,
                                       sio.mem)
            if res['message']['code'] > 0:
                for lp in data['data']['participants']:
                    sid_pa = await get_sid_user(lp,
                                                r=sio.redis_socket)
                    if sid_pa:
                        for s in sid_pa:
                            await sio.emit(socket_removed_messages,
                                           {'data': {
                                               'messages': messages_for_remove
                                           }},
                                           room=s,
                                           namespace=namespace)
            else:
                await sio.emit(socket_error,
                               {'data': res['message']},
                               room=sid,
                               namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on(socket_entry_to_chat, namespace=namespace)
async def entry_chat(sid, message):
    # print('entry')
    try:
        token = message['token']
        is_good, data = token_to_data(token)
        r = await check_valid_data_multi(data['id_user'],
                                         token,
                                         data['role'],
                                         sio.redis_)
        if r:
            chat_id = message['id_chat']
            await entry_to_chat(chat_id,
                                data['id_user'],
                                sio.pool,
                                sio.mem,
                                sio.redis_chat)
            list_sender = await get_sid_user(data['id_user'],
                                             r=sio.redis_socket)
            for s in list_sender:
                    await sio.emit(socket_viewed_dialog,
                                   {'data': {
                                       'id_chat': chat_id,
                                       'id_user': data['id_user']}},
                                   room=s,
                                   namespace=namespace)
        else:
            await sio.emit(socket_error,
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)


@sio.on(socket_you_typing, namespace=namespace)
async def is_typing(sid, message):
    # print('typing')
    try:
        token = message['token']
        is_good, data = token_to_data(token)
        r = await check_valid_data_multi(data['id_user'],
                                         token,
                                         data['role'],
                                         sio.redis_)

        if r:
            chat_id = message['id_chat']
            list_part = await participant_in_dialogue(chat_id,
                                                      data['id_user'],
                                                      cache=sio.mem,
                                                      pool=sio.pool)
            list_part.append(data['id_user'])
            for lp in list_part:
                sid_pa = await get_sid_user(lp,
                                            r=sio.redis_socket)
                for s in sid_pa:
                    await sio.emit(socket_typing,
                                   {'data': {
                                       'id_chat': chat_id,
                                       'id_user': data['id_user'],
                                       'is_typing': bool(message['is_typing'])
                                   }},
                                   room=s,
                                   namespace=namespace)
        else:
            await sio.emit(socket_error,
                           {'data': error_valid_data},
                           room=sid,
                           namespace=namespace)
    except KeyError:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': error_arguments},
                       room=sid,
                       namespace=namespace)
    except:
        traceback.print_exc()
        await sio.emit(socket_error,
                       {'data': unknown_error},
                       room=sid,
                       namespace=namespace)
