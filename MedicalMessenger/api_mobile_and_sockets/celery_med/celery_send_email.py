from MedicalMessenger.api_admin.__init__ import celery
from other_functions.send_email import send_mail


@celery.task
def send_mail_restore(to, title, text):
    send_mail(to, title, text)