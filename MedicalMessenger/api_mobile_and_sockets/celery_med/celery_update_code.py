# from MedicalMessenger.api_mobile_and_sockets import app_celery as celery
from celery import Celery
celery = Celery()
import time
from datetime import datetime, timedelta
from MedicalMessenger.api_mobile_and_sockets.api.redis.redis_chat import set_timestamp_last_update_chat_code,\
    get_timestamp_last_update_chat_code
from MedicalMessenger.api_admin.settings_app import decorator_connect
from MedicalMessenger.api_mobile_and_sockets.helpers.generators import generate_code


@decorator_connect
def get_time_from_db(*args, **kwargs):
    id_chat = args[0]
    select = '''
        SELECT date_last_update_code, ttl_code
        FROM chat
        WHERE id_chat = {}
    '''.format(id_chat)
    conn = kwargs['conn']
    cur = conn.cursor()
    cur.execute(select)
    row = cur.fetchone()
    return row[0], row[1]


@decorator_connect
def set_time_to_db(*args,
                   **kwargs):
    id_chat = args[0]
    timestamp = args[1]
    code = args[2]
    conn = kwargs['conn']
    cur = conn.cursor()

    while True:
        try:
            update = '''
                update chat
                set date_last_update_code = {},
                  code_for_join = '{}'
                where id_chat = {}
            '''.format(timestamp,
                       code,
                       id_chat)
            cur.execute(update)
            break
        except:
            conn.rollback()
            code = generate_code()
    conn.commit()


@celery.task
def update_code(id_chat, timestamp, ttl):
    tim = get_timestamp_last_update_chat_code(id_chat)
    if tim < 0:
        tim, ttl = get_time_from_db(id_chat)
    if tim == timestamp:

        timestamp = int(time.time())
        tomorrow = datetime.utcnow() + timedelta(seconds=ttl)
        set_time_to_db(id_chat,
                       timestamp,
                       generate_code())
        set_timestamp_last_update_chat_code(id_chat,
                                            timestamp)
        update_code.apply_async((id_chat, timestamp, ttl),
                                eta=tomorrow)
