# from MedicalMessenger.api_mobile_and_sockets import app_celery
from celery import Celery
app_celery = Celery()

from pushjack import GCMClient,\
    APNSClient
from MedicalMessenger.config_app import push_key_android,\
    push_key_ios
import traceback


def push(GCMC, message, token_hex):
    resp = GCMC.send(token_hex,
                     #message,
                     message['title'],
                     notification=message,
                     #data=message,
                     # collapse_key='collapse_key',
                     delay_while_idle=True,
                     time_to_live=604800)


def push_android(GCMC, message, token_hex):
    new_message = {
        'header': message['title'],
        'message': message['body'],
        'code': message['code']
    }
    resp = GCMC.send(token_hex,
                     new_message,
                     # message['title'],
                     # notification=message,
                     #data=message,
                     # collapse_key='collapse_key',
                     delay_while_idle=True,
                     time_to_live=604800)


def send_push(data_for_push, message):
    GCMC_android = GCMClient(api_key=push_key_android)
    GCMC_ios = GCMClient(api_key=push_key_ios)
    for l in data_for_push:
        if l[0]:
            try:
                push_android(GCMC_android, message, l[1])
            except:
                GCMC_android = GCMClient(api_key=push_key_android)
                try:
                    push_android(GCMC_android, message, l[1])
                except:
                    GCMC_android = GCMClient(api_key=push_key_android)
        else:
            try:
                push(GCMC_ios, message, l[1])
            except:
                traceback.print_exc()
                GCMC_ios = GCMClient(api_key=push_key_ios)
                try:
                    push(GCMC_ios, message, l[1])
                except:
                    GCMC_ios = GCMClient(api_key=push_key_ios)


@app_celery.task
def send_push_message(id_user, message):
    # user_data = [[is_android, push_key]]
    # message = {
    # 'title': 'Новость',
    # 'body': 'новость',
    # 'code': 2
    #     }
    # send_push(user_data, message)
    pass