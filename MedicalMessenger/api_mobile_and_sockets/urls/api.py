from MedicalMessenger.config_app import domain_api_mobile

api_check_phone = domain_api_mobile + '/check/phone'  # + # +
api_check_code = domain_api_mobile + '/check/code'  # + # +
api_check_password = domain_api_mobile + '/check/password'  # + # +
api_request_new_code = domain_api_mobile + '/request/code'  # + # +
api_restore_password = domain_api_mobile + '/restore/password'  # + # +
api_logout_user = domain_api_mobile + '/logout'  # +

api_get_specialisation = domain_api_mobile + '/specializations'  # + # +
api_get_cities = domain_api_mobile + '/cities'  # + # +
api_update_user = domain_api_mobile + '/update/user'  # + # +
api_upload_photo_profile = domain_api_mobile + '/upload/photo/profile'  # + # +

api_create_chat = domain_api_mobile + '/chat/create'  # + # +
api_upload_photo_chat = domain_api_mobile + '/chat/photo/upload'  # + # +
api_entry_to_chat_by_code = domain_api_mobile + '/chat/entry/by_code'  # + # +
api_create_dialog = domain_api_mobile + '/dialog/create'  # +
api_get_types_sub_chat = domain_api_mobile + '/chat/types/sub/chat'
api_entry_to_sub_chat = domain_api_mobile + '/chat/sub/entry'  # +
api_information_chat = domain_api_mobile + '/chat/information'  # + # +

api_get_list_dialogs = domain_api_mobile + '/chats/{page}'  # + # +
api_get_list_messages = domain_api_mobile + '/messages/{page}'  # + # +
api_update_information_chat = domain_api_mobile + '/chat/information/update'  # +
api_auto_generate_code = domain_api_mobile + '/chat/generate/code'  # +

api_upload_files = domain_api_mobile + '/file/upload'  # +
api_ban_user_chat = domain_api_mobile + '/chat/ban/user'  # + # +

api_get_information_user = domain_api_mobile + '/get_simple_info_user'  # +

api_remove_user_from_chat = domain_api_mobile + '/chat/remove_user'  # + # +
api_add_permission_admin_for_chat = domain_api_mobile + '/chat/update/permission'  # +

api_get_contacts_user_by_chat = domain_api_mobile + '/chat/get/contacts'

api_congratulation_user = domain_api_mobile + '/congratulation/user'
