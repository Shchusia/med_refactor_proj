socket_my_connect = 'conn'
socket_my_disconnect = 'disc'
socket_send_message = 'send_message'
socket_new_message = 'new_message'
socket_error = 'error'
socket_remove_message = 'remove_message'
socket_removed_messages = 'removed_messages'
socket_online_status = 'online_status'
socket_typing = 'typing'
socket_viewed_dialog = 'viewed_dialog'
socket_you_typing = 'is_typing'
socket_entry_to_chat = 'entry_chat'
