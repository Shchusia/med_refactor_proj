good_select = {
    'title': 'успех',
    'body': 'успешный запрос',
    'code': 1
}
good_check_phone = {
    'title': 'успех',
    'body': 'телефон есть в системе',
    'code': 1,
}

sign_in_not_exist_phone = {
    'title': '',
    'body': 'нет такого телефона в системе',
    'code': 1
}

good_sign_in = {
    'title': 'успех',
    'body': 'успешный вход',
    'code': 1,
}

good_get_user = {
    'title': '',
    'body': '',
    'code': 2
}

# ___

sign_in_error_not_exist_phone = {
    'title': 'ошибка входа',
    'body': 'нет такого телефона в системе',
    'code': -2
}

sign_in_error_code_or_phone = {
    'title': 'ошибка входа',
    'body': 'неверный код/пароль или нет такого телефона в системе',
    'code': -3
}

sign_in_error_time_send_sms = {
    'title': 'ошибка входа',
    'body': 'слишком часто хочешь',
    'code': -4
}

sign_in_error_limit_send_sms = {
    'title': 'ошибка входа',
    'body': 'слишком много хочешь',
    'code': -5
}

sign_in_error_phone_not_client = {
    'title': 'ошибка входа',
    'body': 'поверьте вам это не надо',
    'code': -6
}

error_get_country_by_phone = {
    'title': 'ошибка',
    'body': 'нет возможности получить города',
    'code': -7
}

error_not_valid_phone = {
    'title': 'ошибка входа',
    'body': 'поверьте вам это не надо',
    'code': -6
}

error_not_valid_phone_or_email = {
    'title': 'ошибка регистрации',
    'body': 'не валидный телефон или эл.почта',
    'code': -7
}

error_valid_data = {
    'title': 'ошибка регистрации',
    'body': 'не валидный телефон или эл.почта',
    'code': -8
}

error_unknown = {
    'title': 'ошибка',
    'body': 'неизвестная',
    'code': -32
}

error_in_select = {
    'title': 'ошибка в запросе',
    'body': 'ошибка в запросе к базе данных',
    'code': -31
}

error_in_connect = {
    'title': 'ошибка при коннекте',
    'body': 'ошибка попытке подключиться к базе данных',
    'code': -31
}

not_valid_token = {
    'title': 'ошибка токена',
    'body': 'не валидный токен',
    'code': -30,
}

unknown_error = {
    'title': 'неизвестная ошибка',
    'body': 'хз чего',
    'code': -32
}

error_arguments = {
    'title': 'ошибка',
    'body': 'недостаточно вргументов для выполнения запроса',
    'code': -33
}


good_upload_file = {
    'title': 'успех',
    'body': 'файл успешно загружен',
    'code': 4,
}

good_get_specialisation = {
    'title': 'успех',
    'body': 'вы успешно получили специализации',
    'code': 5
}

good_get_cites = {
    'title': 'успех',
    'body': 'вы успешно получили города',
    'code': 6
}

good_restore_password_email = {
    'title': 'успех',
    'body': 'поздравляем, вам на почту выслан новый пароль',
    'code': 7
}

good_restore_password_sms = {
    'title': 'успех',
    'body': 'поздравляем, вам в sms выслан новый пароль',
    'code': 8
}

good_get_chat = {
    'title': 'успех',
    'body': 'вы успешно получили чат',
    'code': 9
}

good_entry_to_chat = {
    'title': 'успех ',
    'body': 'вы успешно присоединились к чату',
    'code': 10
}

good_create_dialog = {
    'title': 'успех',
    'body': 'вы успешно создали диалог',
    'code': 11
}

good_get_dialogs = {
    'title': 'успех',
    'body': 'вы успешно получили диалоги',
    'code': 12
}

good_get_messages = {
    'title': 'успех',
    'body': 'вы успешно получили сообщения',
    'code': 13
}

good_generate_code = {
    'title': 'успех',
    'body': 'вы успешно обновили код',
    'code': 14
}

good_ban_users = {
    'title': 'успех',
    'body': 'вы успешно заблокировали пользователей',
    'code': 15
}

good_remove_user = {
    'title': 'успех',
    'body': 'вы успешно удалили пользователей',
    'code': 16
}

good_set_admin = {
    'title': 'успех',
    'body': 'вы создали адимна',
    'code': 17
}

good_rem_permission = {
    'title': 'успех',
    'body': 'вы лишили прав адимна',
    'code': 18
}

good_get_types_chat = {
    'title': 'успех',
    'body': 'вы успешно получили типы подчатов',
    'code': 19
}

good_sign_out = {
    'title': 'успех',
    'body': 'вы успешно вышли',
    'code': 20
}

good_get_contacts = {
    'title': 'успех',
    'body': 'успешно получили контакты',
    'code': 21
}
# ___________________ #


sign_in_error_phone_or_password = {
    'title': 'ошибка входа',
    'body': 'неверный телефон и/или пароль',
    'code': -5
}


restore_error_not_exist = {
    'title': 'ошибка востановления',
    'body': 'нет такой почты или нет прав доступа',
    'code': -7
}


error_not_your_chat = {
    'title': 'ошибка',
    'body': 'вы не можете создать под чат так как он принадлежит не вам',
    'code': -9
}

error_not_exist_sub_chat_type = {
    'title': 'ошибка',
    'body': 'нет такого типа под чатов',
    'code': -10
}

error_you_not_partner = {
    'title': 'ошибка',
    'body': 'вы не партнер и не можете создавать чаты',
    'code': -11
}

error_not_your_chat_for_view = {
    'title': 'ошибка',
    'body': 'вы не имеете отношения к чату, чтоб просматривать его информацию',
    'code': -12
}

error_not_entry_to_chat_ald_code = {
    'title': 'ошибка',
    'body': 'вы не присоединились ни к одному чату, т.к. код устарел',
    'code': -13
}

error_not_entry_you_in_chat = {
    'title': 'ошибка',
    'body': 'вы не присоединились ни к одному чату, т.к. вы уже в чате которому принадлежит этот код',
    'code': -14
}

error_chat_exist_with_user = {
    'title': 'ошибка',
    'body': 'диалог не создан так как уже существует',
    'code': -15
}

error_not_connect_to_chat = {
    'title': 'ошибка',
    'body': 'вы не присоединились к чату из-за отсутствия прав доступа',
    'code': -16
}

error_not_create_chat_with_your_self = {
    'title': 'ошибка',
    'body': 'вы не создали диалог с собой',
    'code': -17
}

error_entry_chat_exist_in_chat = {
    'title': 'ошибка',
    'body': 'вы не  подключились к чату т.к. уже в нем',
    'code': -18
}

error_not_create_chat_by_not_in_one_chat = {
    'title': 'ошибка',
    'body': 'диалог не создан т.к. нет права доступа',
    'code': -19
}

error_permision = {
    'code': -20,
    'title': 'ошибка',
    'body': 'нет прав на получение сообщений данного чата'
}

error_permission_update_chat = {
    'code': -21,
    'title': 'ошибка',
    'body': 'нет прав на изменения данных чата'
}

error_content_length_greater_then_max_length = {
    'code': -22,
    'title': 'ошибка',
    'body': 'нет прав на получение сообщений данного чата'
}

error_permission_for_bun = {
    'code': -23,
    'title': 'ошибка',
    'body': 'нет прав на блокировку пользователя'
}

error_set_admin = {
    'code': -24,
    'title': 'ошибка',
    'body': 'данный пользователь уже админ'
}

error_rem_permission = {
    'code': -25,
    'title': 'ошибка',
    'body': 'не удалось лишить прав'
}

error_update_permission = {
    'code': -26,
    'title': 'ошибка',
    'body': 'пользователь не относится к чату'
}

error_permission_not_admin = {
    'code': -27,
    'title': 'ошибка',
    'body': 'вы не админ чтоб грузить фотки'
}

arguments_error = {
    'title': 'ошибка',
    'body': 'нет необходимых аргументов для выполнения запроса',
    'code': -30
}
error_permission_remove_user = {
    'code': -23,
    'title': 'ошибка',
    'body': 'нет прав на удаление пользователей из чата'
}

title_email_restore = 'Восстановление пароля в Medical Messenger'
text_email_restore = 'Ваш новый пароль {}'


error_id_city = 'не существует города с таким id'
error_id_specialisation = 'не существует специализации с таким id'
error_valid_date = 'дата не соответствует требуемому формату yyyy-mm-dd'
error_email = 'данная почта используется другим пользователем'
error_not_unique_mail = 'данная почта занята другим пользователем'
error_update_push_data = 'не обновлены данные для пушей'

default_name_dialog = 'Диалог'
