import socketio


class MySio(socketio.AsyncServer):
    pool = None  # pool коннектов к базе postgresql
    mem = None  # асинхронное подкючение к memcached
    rabbit_connect = None  # подключение к очереди

    redis_ = None
    redis_sms = None
    redis_time = None
    redis_socket = None
    redis_contacts = None
    redis_chat = None

    def __init__(self, async_mode='aiohttp'):
        super().__init__(async_mode=async_mode)
