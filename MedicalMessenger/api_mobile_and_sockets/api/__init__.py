from MedicalMessenger.api_mobile_and_sockets.urls.api import *
from MedicalMessenger.api_mobile_and_sockets.api.entry.api_entry import check_phone,\
    check_code, \
    check_password,\
    request_code,\
    get_cities,\
    get_specialisation, \
    update_user,\
    upload_photo_profile,\
    restore_password,\
    get_information_user,\
    logout_user
from MedicalMessenger.api_mobile_and_sockets.api.files.api_file import upload_file
from MedicalMessenger.api_mobile_and_sockets.api.concrete_chat.api_concrete_chat import get_list_dialogs, get_list_messages, remove_user, add_permission_admin
from MedicalMessenger.api_mobile_and_sockets.api.contacts.api_contacts import  get_contacts_for_user
from MedicalMessenger.api_mobile_and_sockets.api.chat.api_chat import create_chat,\
    upload_photo_chat, \
    entry_to_chat,\
    entry_to_sub_chat, \
    create_dialog, \
    update_information_chat,\
    auto_generate_code,\
    ban_user,\
    chat_info,\
    get_types_sub_chat

from MedicalMessenger.config_app import secret_word_for_congratulation



routes = [
    ('POST', api_create_chat, create_chat, 'create_chat'),
    ('POST', api_upload_photo_chat, upload_photo_chat, 'upload_photo_chat'),
    ('POST', api_entry_to_chat_by_code, entry_to_chat, 'entry_to_chat'),
    ('POST', api_entry_to_sub_chat, entry_to_sub_chat, 'entry_to_sub_chat'),
    ('POST', api_create_dialog, create_dialog, 'create_dialog'),
    ('POST', api_update_information_chat, update_information_chat, 'update_information_chat'),
    ('GET', api_auto_generate_code, auto_generate_code, 'auto_generate_code'),
    ('POST', api_ban_user_chat, ban_user, 'ban_user'),
    ('GET', api_information_chat, chat_info, 'chat_info'),
    ('GET', api_get_types_sub_chat, get_types_sub_chat, 'get_types_sub_chat'),
    # dialogs
    ('GET', api_get_list_dialogs, get_list_dialogs, 'get_list_dialogs'),
    ('GET', api_get_list_messages, get_list_messages, 'get_list_messages'),
    ('POST', api_remove_user_from_chat, remove_user, 'remove_user'),
    ('POST', api_add_permission_admin_for_chat, add_permission_admin, 'add_permission_admin'),
    # entry
    ('GET', api_get_contacts_user_by_chat, get_contacts_for_user, 'get_contacts_for_user'),
    ('POST', api_check_phone, check_phone, 'check_phone'),
    ('POST', api_check_code, check_code, 'check_code'),
    ('POST', api_check_password, check_password, 'check_password'),
    ('POST', api_request_new_code, request_code, 'request_code'),
    ('POST', api_update_user, update_user, 'update_user'),
    ('POST', api_get_cities, get_cities, 'get_cities'),
    ('POST', api_upload_photo_profile, upload_photo_profile, 'upload_photo_profil'),
    ('POST', api_restore_password, restore_password, 'restore_password'),
    ('GET', api_get_specialisation, get_specialisation, 'get_specialisation'),
    ('GET', api_get_information_user, get_information_user, 'get_information_user'),
    ('GET', api_logout_user, logout_user, 'logout_user'),
    ('POST', api_upload_files, upload_file, 'upload_file')
]
