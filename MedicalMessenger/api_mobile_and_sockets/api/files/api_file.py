from MedicalMessenger.config_app import MEDIA_FOLDER, \
    domain_photo,\
    str_path_to_file, \
    path_to_other_files,\
    MAX_LENGTH_FILE
import binascii
import os
from aiohttp import web
from MedicalMessenger.api_mobile_and_sockets.answer_server import good_upload_file,\
    arguments_error,\
    error_content_length_greater_then_max_length,\
    not_valid_token
from MedicalMessenger.config_app import START_CODE_FOR_ERROR
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data
from MedicalMessenger.api_mobile_and_sockets.api.redis.redb import check_valid_data_multi

MAX_FILE_SIZE = 1024 * 1024 + 1


async def upload_file(request):
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        r = request.app['redis']
        if is_good and await check_valid_data_multi(data['id_user'], token, data['role'],r):

            di = await request.post()
            filename = di['file'].filename
            file = di['file'].file
            if int(request.headers['Content-Length']) < MAX_LENGTH_FILE:
                if not os.path.exists(MEDIA_FOLDER + str_path_to_file + path_to_other_files):
                    os.makedirs(MEDIA_FOLDER + str_path_to_file + path_to_other_files)
                name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(filename.split('.')[-1])
                out = open(MEDIA_FOLDER + str_path_to_file + path_to_other_files + name, 'wb')
                while True:
                    file_bytes = file.read(MAX_FILE_SIZE)
                    out.write(file_bytes)
                    if len(file_bytes) < MAX_FILE_SIZE:
                        break
                out.close()

                path = domain_photo + str_path_to_file + path_to_other_files + name
                return web.json_response({
                        'code': 0,
                        'message': good_upload_file,
                        'data': {
                            'path_to_file': path
                        }
                })
            return web.json_response({
                'code': 0,
                'message': error_content_length_greater_then_max_length,
                'data': {}
            }, status=START_CODE_FOR_ERROR-error_content_length_greater_then_max_length['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])

    except KeyError:
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR-arguments_error['code'])

