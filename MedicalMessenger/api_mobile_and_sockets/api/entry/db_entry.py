import jwt
from MedicalMessenger.config_app import role_client,\
    role_partner,\
    COUNT_SMS,\
    path_to_default_photo_profile
from MedicalMessenger.api_mobile_and_sockets.answer_server import good_check_phone,\
    sign_in_error_code_or_phone,\
    sign_in_error_time_send_sms,\
    sign_in_error_limit_send_sms,\
    sign_in_error_phone_not_client, \
    good_get_user,\
    good_sign_in,\
    good_get_specialisation,\
    good_get_cites,\
    error_get_country_by_phone,\
    good_select,\
    restore_error_not_exist,\
    text_email_restore,\
    title_email_restore,\
    good_restore_password_email,\
    sign_in_not_exist_phone,\
    sign_in_error_phone_or_password,\
    good_restore_password_sms
from MedicalMessenger.api_mobile_and_sockets.api.redis.redis_sms import get_count_code,\
    check_phone,\
    get_time_,\
    set_phone,\
    add_count_code,\
    get_time_code
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_user import set_information_user,\
    get_information_user
from MedicalMessenger.global_functions.send_email import send_mail
from MedicalMessenger.api_mobile_and_sockets.helpers.generators import data_to_token, \
    create_hash_password


async def db_get_user(id_user,
                      pool,
                      cache):
    async def tmp(id_user,
                  pool,
                  cache):
        async with pool.acquire() as conn:

            select = '''
                    SELECT u.id_user, u.phone, u.path_img, u.full_name, u.ref_id_role,u.email
                    FROM users u
                    WHERE u.id_user = {}
                '''.format(id_user)
            row = await conn.fetchrow(select)
            user = {
                'id_user': row[0],
                'phone': row[1],
                'full_name': row[3],
                'email': row[5]

            }
            select = '''
                    SELECT  c.city_title, r.region_
                    FROM users u, city c, region r
                    WHERE u.id_user = {}
                    AND u.ref_id_city = c.id_city
                    AND c.ref_id_region = r.id_region
                '''.format(row[0])
            row_c = await conn.fetchrow(select)
            if row_c:
                user['city_title'] = row_c[0]
                user['region'] = row_c[1]
            else:
                user['city_title'] = ''
                user['region'] = ''

            if row[4] == 3:
                user['role'] = role_client
                select = '''
                        SELECT specialisation_
                        FROM users, specialisation
                        WHERE ref_id_specialisation = specialisation.id_specialisation
                        AND id_user = {}
                    '''.format(id_user)
                row_w = await conn.fetchrow(select)
                try:
                    user['specialisation'] = row_w[0]
                except:
                    user['specialisation'] = 0
            else:
                if row[4] == 2:
                    user['role'] = role_partner
                else:
                    user['role'] = role_partner
                user['specialisation'] = role_partner
            if row[2]:
                user['path_img'] = row[2]
            else:
                user['path_img'] = path_to_default_photo_profile
            await set_information_user(id_user, user, cache)  # запись в мемкэш

            return user

    user_ = await get_information_user(id_user, cache)
    if user_:
        pass
    else:
        user_ = await tmp(id_user,
                          pool,
                          cache)
    return {
        'code': 1,
        'message': good_get_user,
        'data': {
            'user': user_
        }
    }


async def db_request_code(phone,
                          pool,
                          redis_time,
                          redis_sms):
    """
    запросить код в сообщении для входа
    """

    async with pool.acquire() as conn:
        select = '''
                SELECT id_user, phone, ref_id_role, full_name
                FROM users
                WHERE phone = '{}'
            '''.format(phone)
        row = await conn.fetchrow(select)
        if row:
            is_new = False
            if row[3] is None:
                is_new = True
            if row[2] == 3:
                if (await check_phone(phone,
                                      redis_time)):
                    if (await get_count_code(phone,
                                             redis_sms)) < COUNT_SMS:
                        code = '000000'  # временный код
                        update = '''
                                    UPDATE user_data
                                    SET code_message = '{}'
                                    WHERE ref_id_user = {}
                                '''.format(code,
                                           row[0])
                        await conn.execute(update)
                        await set_phone(phone, redis_time)
                        await add_count_code(phone, redis_sms)
                        '''send_sms(phone,
                                         text_sms_message.format(code), 
                                         title_sms)
                                '''
                        return {
                            'code': 1,
                            'message': good_check_phone,
                            'data': {
                                'role': role_client,
                                'left_time': (await get_time_(phone,
                                                              redis_time)),
                                'left_sms':  (COUNT_SMS - (await get_count_code(phone,
                                                                                redis_sms))),
                                'is_new': is_new
                                }
                            }
                    return {
                        'code': 1,
                        'message': sign_in_error_limit_send_sms,
                        'data': {
                            'role': role_client,
                            'left_time': (await (get_time_code(phone,
                                                               redis_sms))),
                            'left_sms': -1,
                            'is_new': is_new,
                        }
                    }
                return {
                    'code': 1,
                    'message': sign_in_error_time_send_sms,
                    'data': {
                        'role': role_client,
                        'left_time': (await get_time_(phone,
                                                      redis_time)),
                        'left_sms': (COUNT_SMS - (await get_count_code(phone,
                                                                       redis_sms))),
                        'is_new': False
                    }
                }
        return {
            'code': 1,
            'message': sign_in_error_phone_not_client,
            'data': {}

        }


async def db_check_phone(phone,
                         pool,
                         redis_time,
                         redis_sms):
    """
    проверка наличия телефона в системе если есть то возвращает роль
    если нет то регистрирует как клиента

    """

    async with pool.acquire() as conn:
        select = '''
            SELECT id_user, phone, ref_id_role
            FROM users
            WHERE phone = '{}'
        '''.format(phone)
        row = await conn.fetchrow(select)
        if row:
            if row[1] == phone:
                if row[2] == 3:
                    return await db_request_code(phone,
                                                 pool,
                                                 redis_time,
                                                 redis_sms)
                return {
                    'code': 1,
                    'message': good_check_phone,
                    'data': {
                        'role': role_partner,
                        'is_new': False

                    }
                }
        registration_user = '''
            INSERT INTO users (id_user, ref_id_role, phone) VALUES (DEFAULT, 3, '{}') returning id_user
        '''.format(phone)
        row = await conn.fetchrow(registration_user)
        id_user = row[0]
        data_user = '''
            INSERT INTO user_data (id_user_data, ref_id_user) VALUES (DEFAULT, {});
            INSERT INTO user_to_chat (ref_id_user, ref_id_chat, is_get_notification, created_on, is_ban) 
            VALUES ({}, 0, TRUE , DEFAULT , FALSE); 
        '''.format(id_user, id_user)
        await conn.execute(data_user)
        data = await db_request_code(phone,
                                     pool,
                                     redis_time,
                                     redis_sms)
        if data['message']['code'] > 0:

            return {
                'code': 1,
                'message': sign_in_not_exist_phone,
                'data': {
                    'role': role_client,
                    'is_new': True,
                    'left_time': data['data']['left_time'],
                    'left_sms': data['data']['left_sms']}
            }
        raise AttributeError


async def db_check_code_sms(phone,
                            code,
                            pool,
                            cache):
    '''
    проверка кода введенного клиентом для входа

    '''
    async with pool.acquire() as conn:
        select = '''
            SELECT id_user, code_message, phone
            FROM users, user_data
            WHERE phone = '{}'
            AND id_user = ref_id_user
            AND code_message = '{}'
        '''.format(phone, code)
        row = await conn.fetchrow(select)
        if row:
            if row[1] == code and row[2] == phone:
                select = '''
                    UPDATE user_data 
                    SET code_message = NULL 
                    WHERE ref_id_user = {}
                '''.format(row[0])
                await conn.execute(select)
                user_data = await db_get_user(row[0],
                                              pool,
                                              cache)
                if user_data['message']['code'] > 0:
                    user = user_data['data']['user']
                    token = data_to_token(user)

                    return {
                        'code': 1,
                        'message': good_sign_in,
                        'data': {
                            'user': user
                        },
                        'token': token
                    }
        return {
            'code': 1,
            'message': sign_in_error_code_or_phone,
            'data': {}
        }


async def db_check_password(phone,
                            passwor,
                            pool,
                            cache):
    '''
    проверка пароля введенного партнером
    '''
    async with pool.acquire() as conn:
        select = '''
            SELECT id_user, password_hash, phone
            FROM users
            WHERE phone = '{}'
            AND password_hash = '{}'
        '''.format(phone,
                   passwor)
        row = await conn.fetchrow(select)
        if row:
            if str(row[1]) == (passwor) and row[2] == phone:
                user_data = await db_get_user(row[0],
                                              pool,
                                              cache)
                if user_data['message']['code'] > 0:
                    user = user_data['data']['user']
                    token = data_to_token(user)
                    return {
                        'code': 1,
                        'message': good_sign_in,
                        'data': {
                            'user': user
                        },
                        'token': token
                    }
                return user_data
        return {
            'code': 1,
            'message': sign_in_error_phone_or_password,
            'data': {}
        }


async def db_get_specialisation(pool):

    async with pool.acquire() as conn:

        select = '''
            SELECT id_specialisation, specialisation_
            FROM specialisation
        '''
        rows = await conn.fetch(select)

        result = [
            {
                'id_specialisation': r[0],
                'specialisation': r[1]
            }
            for r in rows
        ]
        return {
            'code': 1,
            'message': good_get_specialisation,
            'data': {
                'specializations': result
            }
        }


async def get_country_by_phone(phone,
                               cur):
    """
    метод определяющий страну по номеру телефона
    :param phone: телефон пользователя
    :param cur: курсор для выполнения запроса к бд
    :return:
    """
    select = '''
        SELECT id_country, title_country, start_num
        FROM country
    '''
    rows = await cur.fetch(select)
    for r in rows:
        if str(r[2]) == str(phone)[:len(r[2])]:
            return r[0], r[1]
    return 0, None


async def db_get_cities(phone, pool):
    """
    получение городов по номеру телефона

    """
    async with pool.acquire() as conn:

        id_country, title_country = await get_country_by_phone(phone,
                                                               conn)
        if id_country > 0:
            select = '''
                SELECT id_city, city_title, is_primary_city
                FROM city, region
                WHERE ref_id_country = {}
                AND ref_id_region = id_region
            '''.format(id_country)
            rows = await conn.fetch(select)
            primary_cites = []
            cites = []

            for r in rows:
                if r[2]:
                    primary_cites.append({
                        'id_city': r[0],
                        'city': r[1]
                    })
                else:
                    cites.append({
                        'id_city': r[0],
                        'city': r[1]
                    })
            return {
                'code': 1,
                'message': good_get_cites,
                'data': {
                    'primary_cities': primary_cites,
                    'cities': cites,
                    'country': title_country
                }
            }

        return {
            'code': 1,
            'message': error_get_country_by_phone,
            'data': {}

        }


async def is_exist_city(id_city,
                        pool):
    """
    проверка наличия города
    """
    async with pool.acquire() as conn:

        select = '''
            SELECT id_city
            FROM city
            WHERE id_city = {}
        '''.format(id_city)
        row = await conn.fetchrow(select)
        if row:
            return {
                'message': {'code': 1}
            }
        else:
            return {
                'message': {'code': -1}
            }


async def is_exist_specialisation(id_specialisation,
                                  pool):
    """
    проверка существвования специализации
    """
    async with pool.acquire() as conn:

        select = '''
            SELECT id_specialisation
            FROM specialisation
            WHERE id_specialisation = {}
        '''.format(id_specialisation)
        row = await conn.fetchrow(select)

        if row:
            return {
                'message': {'code': 1}
            }
        else:
            return {
                'message': {'code': -1}
            }


async def db_check_exist_email(email,
                               pool):
    '''
    метод проверкии наличия почты в системе
    '''
    async with pool.acquire() as conn:

        select = '''
            SELECT id_user, email
            FROM users
            WHERE email = '{}'
        '''.format(email)
        row = await conn.fetchrow(select)

        if row:
            if row[1] == email:
                return row[0]
        return 0


async def db_update_user(select,
                         id_user,
                         pool,
                         cache):
    """
    метод обновления пользователя

    """
    async with pool.acquire() as conn:
        await conn.execute(select)
        await set_information_user(id_user,
                                   None,
                                   cache)

        return {
            'code': 1,
            'message': good_select,
            'data': {}
        }


async def db_update_push_data(is_ios,
                              push_key,
                              device_id,
                              id_user,
                              pool):
    """
    обновление пуш данных пользователся
    """

    async with pool.acquire() as conn:

    # cur = conn.cursor()
        select = '''
            INSERT INTO user_data (id_user_data, ref_id_user, is_ios, push_key, device_id)
            VALUES (DEFAULT, {}, {}, '{}', '{}')
        '''.format(id_user, is_ios, push_key, device_id)
        update = '''
            UPDATE user_data
            SET ref_id_user = {},
            push_key = '{}',
            is_ios = {}
            WHERE device_id = '{}'
        '''.format(id_user,
                   push_key,
                   is_ios,
                   device_id)

        async with conn.transaction():
            try:
                await conn.execute(select)
            except:
                await conn.rollback()
                await conn.execute(update)

        return {
            'code': 1,
            'message': good_get_user,
            'data': {}
        }


async def db_restore_password(phone,
                              pool):
    """
    запрос на замену пароля(партнер админ)

    """
    async with pool.acquire() as conn:

        select = '''
            SELECT id_user, phone,email
            FROM users
            WHERE phone = '{}'
            AND (ref_id_role = 1 OR ref_id_role = 2)
        '''.format(phone)
        row = await conn.fetchrow(select)
        if row:
            if row[1] == phone:
                # new_password = generate_code()
                new_password = '000000'
                new_hash = create_hash_password(new_password)
                update = '''
                    UPDATE users 
                    SET password_hash = '{}'
                    WHERE id_user = {}
                    AND phone = '{}'
                '''.format(new_hash,
                           row[0],
                           phone)

                await conn.execute(update)
                if row[2]:
                    # send_mail_restore.delay(email, title_email_restore, text_email_restore.format(new_password))
                    send_mail(row[2], title_email_restore, text_email_restore.format(new_password))
                    message = good_restore_password_email
                else:
                    message = good_restore_password_sms
                    '''send_sms(phone,
                             text_sms_message_restore.format(new_password),
                             title_sms_restore)'''

                return {
                    'code': 1,
                    'message': message,
                    'data': {}

                }

        return {
            'code': 1,
            'message': restore_error_not_exist,
            'data': {}
        }

