from MedicalMessenger.api_mobile_and_sockets.answer_server import arguments_error,\
    error_not_valid_phone,\
    error_email,\
    error_id_city, \
    error_id_specialisation,\
    error_not_unique_mail,\
    error_update_push_data,\
    error_unknown,\
    good_upload_file,\
    not_valid_token,\
    good_sign_out,\
    error_valid_date
from MedicalMessenger.config_app import START_CODE_FOR_ERROR,\
    START_CODE_FOR_GOOD,\
    MEDIA_FOLDER,\
    str_path_to_file,\
    path_to_photo_profile,\
    domain_photo,\
    role_client
from MedicalMessenger.api_mobile_and_sockets.api.entry.db_entry import db_check_password,\
    db_check_phone,\
    db_check_code_sms,\
    db_request_code,\
    db_get_specialisation,\
    db_get_cities,\
    is_exist_specialisation,\
    is_exist_city,\
    db_check_exist_email,\
    db_get_user,\
    db_update_user,\
    db_update_push_data,\
    db_restore_password
from MedicalMessenger.global_functions.compress_photo import comppress_photo
from MedicalMessenger.api_mobile_and_sockets.api.redis.redb import set_client_multi,\
    remove_user,\
    check_valid_data_multi,\
    sign_out_multi
import os
import binascii
import traceback
from aiohttp import web
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import is_valid_phone, \
    check_email, \
    is_valid_date, \
    phone_to_easy_phone
from MedicalMessenger.api_mobile_and_sockets.helpers.generators import data_to_token
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data

MAX_FILE_SIZE = 1024 * 1024 + 1


async def check_phone(request):
    pool = request.app['pool']  # пул connect to db

    r_s = request.app['redis_sms']
    r_t = request.app['redis_time']
    try:
        di = dict(await request.post())
        phone = await phone_to_easy_phone(di['phone'])
        if await is_valid_phone(phone):
            data = await db_check_phone(phone,
                                        pool,
                                        r_t,
                                        r_s)
            if data['message']['code'] > 0:
                return web.json_response(data,
                                         status=START_CODE_FOR_GOOD)
            return web.json_response(data,
                                     status=START_CODE_FOR_ERROR - data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_valid_phone,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_not_valid_phone['code'])

    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def check_code(request):
    pool = request.app['pool']  # пул connect to db
    r = request.app['redis']
    cache = request.app['mem']
    try:
        di = dict(await request.post())
        # print(di)
        phone = await phone_to_easy_phone(di['phone'])
        code = di['code']
        if await is_valid_phone(phone):
            data = (await db_check_code_sms(phone,
                                            code,
                                            pool,
                                            cache))
            if data['message']['code'] > 0:
                token = data['token']
                del data['token']
                await set_client_multi(data['data']['user']['id_user'],
                                       token,
                                       data['data']['user']['role'],
                                       r)
                return web.json_response(data, headers={'Token': token})
            return web.json_response(data, status=START_CODE_FOR_ERROR - data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_valid_phone,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_not_valid_phone['code'])

    except KeyError:
        return web.json_response({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def check_password(request):
    pool = request.app['pool']  # пул connect to db
    r = request.app['redis']
    cache = request.app['mem']
    try:
        di = dict(await request.post())
        # print(di)
        phone = await phone_to_easy_phone(di['phone'])
        if await is_valid_phone(phone):
            data = await db_check_password(phone,
                                           di['password_hash'],
                                           pool,
                                           cache)
            if data['message']['code'] > 0:
                token = data['token']
                del data['token']

                set_client_multi(data['data']['user']['id_user'],
                                 token,
                                 data['data']['user']['role'],
                                 r)
                return web.json_response(data,
                                         headers={'Token': token})
            return web.json_response(data,
                                     status=START_CODE_FOR_ERROR - data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_valid_phone,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_not_valid_phone['code'])

    except KeyError:
        return web.json_response({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def request_code(request):
    pool = request.app['pool']  # пул connect to db
    r_s = request.app['redis_sms']
    r_t = request.app['redis_time']
    try:
        di = dict(await request.post())
        phone = await phone_to_easy_phone(di['phone'])
        if await is_valid_phone(phone):
            data = await db_request_code(phone,
                                         pool,
                                         redis_time=r_t,
                                         redis_sms=r_s)
            if data['message']['code'] > 0:
                return web.json_response(data)
            return web.json_response(data,
                                     status=START_CODE_FOR_ERROR - data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_valid_phone,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_not_valid_phone['code'])

    except KeyError:
        return web.json_response({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def get_cities(request):
    pool = request.app['pool']  # пул connect to db
    try:
        di = dict(await request.post())
        phone = await phone_to_easy_phone(di['phone'])
        if await is_valid_phone(phone):
            data = await db_get_cities(phone, pool)
            code = START_CODE_FOR_ERROR - data['message']['code']
            if data['message']['code'] > 0:
                code = START_CODE_FOR_GOOD
            return web.json_response(data, status=code)
        return web.json_response({
            'code': 1,
            'message': error_not_valid_phone,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_not_valid_phone['code'])

    except KeyError:
        return web.json_response({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def get_specialisation(request):
    pool = request.app['pool']  # пул connect to db
    data = await db_get_specialisation(pool)
    code = START_CODE_FOR_ERROR - data['message']['code']
    if data['message']['code'] > 0:
         code = START_CODE_FOR_GOOD
    return web.json_response(data, status=code)


async def update_user(request):
    pool = request.app['pool']  # пул connect to db
    r = request.app['redis']
    cache = request.app['mem']
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if (await check_valid_data_multi(data['id_user'],
                                             token,
                                             data['role'],
                                             r)):
                di = dict(await request.post())
                messages = []
                select = '''
                    UPDATE users 
                    SET {}
                    WHERE id_user = {}
                '''
                val = []
                try:
                    is_ios = di['is_ios']
                    push_key = di['push_key']
                    device_id = di['device_id']
                    answer = await db_update_push_data(is_ios,
                                                       push_key,
                                                       device_id,
                                                       data['id_user'],
                                                       pool)
                    if answer['message']['code'] > 0:
                        pass
                    else:
                        messages.append(error_update_push_data)
                except KeyError:
                    pass
                try:
                    full_name = di['full_name']
                    val.append("full_name = '{}'".format(full_name))
                except KeyError:
                    pass
                try:
                    email = di['email']
                    if await check_email(email):
                        id_ = await db_check_exist_email(email,pool)
                        if id_ == data['id_user'] or id_ == 0:
                            val.append(" email = '{}'".format(email))
                        else:
                            messages.append(error_email)
                    else:
                        messages.append(error_not_unique_mail)
                except KeyError:
                    pass
                try:
                    id_city = di['id_city']
                    if (await is_exist_city(id_city,
                                            pool))['message']['code'] > 0:
                        val.append("ref_id_city = {}".format(id_city))
                    else:
                        messages.append(error_id_city)
                except KeyError:
                    pass
                try:
                    id_specialisation = di['id_specialisation']
                    if (await is_exist_specialisation(id_specialisation,
                                                      pool))['message']['code'] > 0:
                        val.append("ref_id_specialisation = {}".format(id_specialisation))
                    else:
                        messages.append(error_id_specialisation)
                except KeyError:
                    pass
                try:
                    birth_day = di['birth_day']
                    if is_valid_date(birth_day):
                        val.append(" birth_day = '{}'".format(birth_day))
                    else:
                        messages.append(error_valid_date)
                except KeyError:
                    pass
                if val:
                    res = await db_update_user(select.format(','.join(val),
                                                             data['id_user']),
                                               data['id_user'],
                                               pool,
                                               cache)
                    if res['message']['code'] < 0:
                        messages.append(res['message']['body'])

                try:
                    password_hash = di['password_hash']
                    update = '''
                        UPDATE users
                        SET password_hash = '{}'
                        WHERE (ref_id_role = 2 or ref_id_role = 1)
                        AND id_user = {}
                    '''.format(password_hash,
                               data['id_user'])
                    data_ = await db_get_user(data['id_user'],
                                              pool,
                                              cache)
                    token = ''
                    if data['role'] != role_client:
                        await db_update_user(update,
                                             data['id_user'],
                                             pool,
                                             cache)
                        await remove_user(data['id_user'],
                                          data['role'],
                                          r)
                        token = data_to_token(data_['data']['user'])
                        await set_client_multi(data_['data']['user']['id_user'],
                                               token,
                                               data_['data']['user']['role'],
                                               r)
                    else:
                        assert ValueError
                except:
                    traceback.print_exc()
                    data_ = await db_get_user(data['id_user'],
                                              pool,
                                              cache)
                    token = ''
                if data_['message']['code'] > 0:

                    if token:
                        answer = {
                            'Token': token}
                    else:
                        answer = {
                            'Token': request.headers['Token']
                        }

                    if messages:
                        mes = data_['message']['body'] + ','.join(messages)
                        data_['message']['body'] = mes
                        ans = web.json_response({
                            'code': 1,
                            'message': data_['message'],
                            'data': data_['data']
                        }, headers=answer)
                    else:
                        ans = web.json_response({
                            'code': 1,
                            'message': data_['message'],
                            'data': data_['data']
                        }, headers=answer)

                    return ans

                else:
                    if token:
                        ans = {
                            'Token': token}
                    else:
                        ans = {
                            'Token': request.headers['Token']
                        }
                    if messages:
                        mes = data_['message']['body'] + ','.join(messages)
                        data_['message']['body'] = mes
                        answer = web.json_response({
                            'code': 1,
                            'message': data_['message'],
                            'data': data_['data']
                        }, headers=ans)
                    else:
                        answer = web.json_response({
                            'code': 1,
                            'message': data_['message'],
                            'data': data_['data']
                        }, headers=ans)
                    return answer
            return web.json_response({
                'code': -1,
                'message': not_valid_token,
                'data': {}
            }, status=START_CODE_FOR_ERROR-not_valid_token['code'])
    except:
            traceback.print_exc()
            return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR-error_unknown['code'])


async def upload_photo_profile(request):
    pool = request.app['pool']  # пул connect to db
    r = request.app['redis']
    cache = request.app['mem']
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if (await check_valid_data_multi(data['id_user'],
                                             token,
                                             data['role'],
                                             r)):
                di = (await request.post())
                filename = di['file'].filename
                file = di['file'].file
                if not os.path.exists(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile):
                    os.makedirs(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile)
                name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(filename.split('.')[-1])
                out = open(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile + name, 'wb')
                while True:
                    file_bytes = file.read(MAX_FILE_SIZE)
                    out.write(file_bytes)
                    if len(file_bytes) < MAX_FILE_SIZE:
                        break
                out.close()
                comppress_photo(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile + name)
                path = \
                    domain_photo +\
                    str_path_to_file + \
                    path_to_photo_profile + \
                    '.'.join(name.split('.')[:-1]) + \
                    '.jpeg'
                try:
                    update_user_photo = '''
                        UPDATE users
                        SET path_img = '{}'
                        WHERE id_user = {}
                    '''.format(path, data['id_user'])
                    await db_update_user(update_user_photo,
                                         data['id_user'],
                                         pool,
                                         cache)
                except:
                    pass

                return web.json_response({
                        'code': 0,
                        'message': good_upload_file,
                        'data': {
                            'path_to_file': path
                        }
                })
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
            traceback.print_exc()
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def restore_password(request):
    pool = request.app['pool']  # пул connect to db
    try:
        di = await request.post()
        phone = await phone_to_easy_phone(di['phone'])
        if await is_valid_phone(phone):
            data = await db_restore_password(phone,
                                             pool)
            if data['message']['code'] > 0:
                return web.json_response(data,
                                         status=START_CODE_FOR_GOOD)
            return web.json_response(data,
                                     status=START_CODE_FOR_ERROR - data['message']['code'])
        return web.json_response({
            'code': 1,
            'message': error_not_valid_phone,
            'data': {}
        }, status=START_CODE_FOR_ERROR-error_not_valid_phone['code'])
    except KeyError:
        return web.json_response({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR-arguments_error['code'])


async def get_information_user(request):
    pool = request.app['pool']  # пул connect to db
    r = request.app['redis']
    cache = request.app['mem']
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if (await check_valid_data_multi(data['id_user'],
                                             token,
                                             data['role'],
                                             r)):
                return web.json_response(await db_get_user(request.headers['Id-User'],
                                                           pool,
                                                           cache))
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR-not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR-arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR-error_unknown['code'])


async def logout_user(request):
    r = request.app['redis']
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        await sign_out_multi(data['id_user'],
                             token,
                             data['role'],
                             r)
        return web.json_response({
            'code': -1,
            'message': good_sign_out,
            'data': {}
        })
    except KeyError:
            traceback.print_exc()
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])


