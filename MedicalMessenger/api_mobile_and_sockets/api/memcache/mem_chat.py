
async def set_information_chat(id_chat, data, cache):
    await cache.set('information_chat_{}'.format(id_chat), data)


async def get_information_chat(id_chat, cache):
    try:
        return await cache.get('information_chat_{}'.format(id_chat))
    except:
        return None


async def set_primary_chat_partner(id_user, id_chat, cache):
    try:
        chats = (await cache.get('primary_partner_{}'.format(id_user)))
        if chats is None:
            chats = []
    except:
        chats = []
    chats.append(id_chat)
    await cache.set('primary_partner_{}'.format(id_user), list(set(chats)))


async def is_exist_primary_chat_partner(id_user, id_chat, cache):
    try:
        chats = (await cache.get('primary_partner_{}'.format(id_user)))
        for c in chats:
            if int(c) == int(id_chat):
                return True
        return False
    except:
        return None


async def set_type_sub_chat(id_type, cache):
    try:
        chats = (await cache.get('types_sub_chat'))
        if chats is None:
            chats = []
    except:
        chats = []
    chats.append(id_type)
    await cache.set('types_sub_chat', list(set(chats)))


async def is_exist_type_sub_chat(id_type, cache):
    try:
        chats = (await cache.get('types_sub_chat'))
        if chats:
            for c in chats:
                if int(c) == int(id_type):
                    return True
            return False
        return None
    except:
        return False


async def set_chat_user(id_user, id_chat, cache):
    try:
        chats = (await cache.get('chats_user_{}'.format(id_user)))
        if chats is None:
            chats = []
    except:
        chats = []
    (chats.append(id_chat))
    await cache.set('chats_user_{}'.format(id_user), list(set(chats)))


async def set_list_chat_to_user(id_user, list_, cache):
    await cache.set('chats_user_{}'.format(id_user), list_)


async def get_chats_user(id_user, cache):
    return await cache.get('chats_user_{}'.format(id_user))


async def set_to_chats(id_chat,cache):
    chats = (await cache.get('chats'))
    if chats is None:
        chats = []
    chats.append(id_chat)
    await cache.set('chats', chats)


async def set_list_chat(list_, cache):
    await cache.set('chats', list_)


async def get_chats(cache):
    return await cache.get('chats')



# set_list_chat([])


if __name__ == '__main__':
    set_list_chat_to_user(1, None)