
async def set_information_user(id_user, data, cache):
    await cache.set('information_user_{}'.format(id_user), data)


async def get_information_user(id_user, cache):
    try:
        return await cache.get('information_user_{}'.format(id_user))
    except:
        return None