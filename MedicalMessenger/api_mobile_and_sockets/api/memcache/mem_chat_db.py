async def set_count_messages(id_user, id_chat, count, cache):
    await cache.set('count_message_{}_in_chat_{}'.format(id_user, id_chat), count)


async def get_count_messages(id_user, id_chat, cache):
    return await cache.get('count_message_{}_in_chat_{}'.format(id_user, id_chat))


async def add_new_message(id_user, id_chat, cache):
    count_ = await get_count_messages(id_user, id_chat, cache)
    if count_:
        count_ += 1
    else:
        count_ = 1
    await set_count_messages(id_user, id_chat, count_, cache)


async def set_list_chat_to_user(id_user, list_, cache):
    await cache.set('chats_user_{}'.format(id_user), list_)


async def get_chats_user(id_user, cache):
    return await cache.get('chats_user_{}'.format(id_user))


async def add_new_chat_to_user(id_user, id_chat, cache):
    chats = await get_chats_user(id_user, cache)
    if chats:
        chats.append(id_chat)
    else:
        chats = [id_chat]
    await set_list_chat_to_user(id_user, list(set(chats)), cache)


async def remove_chat_on_user(id_chat, id_user, cache):
    chats = await get_users_chat(id_user, cache)
    if chats:
        for i, c in enumerate(chats):
            if int(c) == int(id_chat):
                del chats[i]
    await set_users_to_chat(id_user, chats, cache)


async def get_users_chat(id_chat, cache):
    return await cache.get('users_on_chat_{}'.format(id_chat))


async def set_users_to_chat(id_chat, list_users, cache):
    await cache.set('users_on_chat_{}'.format(id_chat), list_users)


async def add_user_to_chat(id_chat, id_user, cache):
    users = await  get_users_chat(id_chat, cache)
    if users is not None:

        list(set(users.append(id_user)))
        await set_users_to_chat(id_chat, users, cache)
    else:
        await set_users_to_chat(id_chat, [id_user], cache)


async def remove_user_from_chat(id_chat, id_user, cache):
    users = await get_users_chat(id_chat, cache)
    if users:
        for i, u in enumerate(users):
            if int(u) == int(id_user):
                del users[i]
    await set_users_to_chat(id_chat, users, cache)
