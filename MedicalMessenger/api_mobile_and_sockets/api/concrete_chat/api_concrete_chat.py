
from aiohttp import web
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data
from MedicalMessenger.config_app import START_CODE_FOR_ERROR
from MedicalMessenger.api_mobile_and_sockets.answer_server import arguments_error, \
    error_unknown,\
    not_valid_token,\
    error_permission_remove_user
from MedicalMessenger.api_mobile_and_sockets.api.concrete_chat.db_concrete_chat import db_get_chats_user_with_messages,\
    db_get_messages_chat, \
    db_remove_users,\
    db_add_permission
from MedicalMessenger.api_mobile_and_sockets.api.redis.redb import check_valid_data_multi
from MedicalMessenger.api_mobile_and_sockets.api.chat.db_chat import is_chat_user,\
    is_admin_chat
import traceback


async def get_list_dialogs(request):
    try:
        pool = request.app['pool']  # пул connect to db
        r_s = request.app['redis_sms']
        r_t = request.app['redis_time']
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            data_ = await db_get_chats_user_with_messages(data['id_user'],
                                                          int(request.match_info['page']),
                                                          pool,
                                                          cache)
            if data_['message']['code'] > 0:
                return web.json_response(data_)
            return web.json_response(data_,
                                     status=START_CODE_FOR_ERROR - data_['message']['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        },status=START_CODE_FOR_ERROR-not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def get_list_messages(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            id_chat = request.headers['id_chat']
            data = await db_get_messages_chat(data['id_user'],
                                              int(request.match_info['page']),
                                              id_chat,
                                              pool,
                                              cache)
            if data['message']['code'] > 0:
                return web.json_response(data)
            return web.json_response(data,
                                     status=START_CODE_FOR_ERROR - data['message']['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def remove_user(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            id_chat = (await request.post())['id_chat']
            if await is_admin_chat(id_chat,
                                   data['id_user'],
                                   pool):
                list_users = list(map(str, (await request.post()).getall('users')))
                data = await db_remove_users(id_chat,
                                             data['id_user'],
                                             list_users,
                                             pool,
                                             cache)
                if data['message']['code'] > 0:
                    return web.json_response(data)
                return web.json_response(data,
                                         status=START_CODE_FOR_ERROR - data['message']['code'])
            return web.json_response({
                'code': 1,
                'message': error_permission_remove_user,
                'data': {}
            },
                status=START_CODE_FOR_ERROR-error_permission_remove_user['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        },
            status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        },
            status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        },
            status=START_CODE_FOR_ERROR - error_unknown['code'])


async def add_permission_admin(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            id_chat = (await request.post())['id_chat']
            id_user = (await request.post())['id_user']
            is_add = str((await request.post())['is_add']).lower()
            if is_add == 'true':
                is_add = True
            else:
                is_add = False
            if await is_chat_user(id_chat,
                                  data['id_user'],
                                  pool):
                data = await db_add_permission(id_user,
                                               id_chat,
                                               is_add,
                                               pool,
                                               cache)
                if data['message']['code'] > 0:
                    return web.json_response(data)
                return web.json_response(data,
                                         status=START_CODE_FOR_ERROR - data['message']['code'])
            return web.json_response({
                'code': 1,
                'message': error_permission_remove_user,
                'data': {}
            },
                status=START_CODE_FOR_ERROR-error_permission_remove_user['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        },
            status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        },
            status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        },
            status=START_CODE_FOR_ERROR - error_unknown['code'])

