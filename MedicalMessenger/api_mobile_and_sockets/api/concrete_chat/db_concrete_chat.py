from MedicalMessenger.config_app import LIMIT_DIALOGS_ON_PAGE,\
    LIMIT_MESSAGES_ON_PAGE
from MedicalMessenger.api_mobile_and_sockets.api.chat.db_chat import db_get_user, \
    db_get_data_chat
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat import set_list_chat_to_user,\
    get_chats_user
from MedicalMessenger.api_mobile_and_sockets.answer_server import good_get_dialogs,\
    error_permision,\
    good_get_messages,\
    good_remove_user,\
    good_set_admin, \
    error_set_admin,\
    good_rem_permission,\
    error_rem_permission,\
    error_update_permission
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat_db import set_count_messages,\
    get_count_messages
from MedicalMessenger.api_mobile_and_sockets.socket.memdb.mem_socket import remove_users_from_chat
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat_db import remove_chat_on_user,\
    remove_user_from_chat
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat import set_information_chat


async def get_l(cur_,
                func_mem,
                fun_db,
                id_user=None):
    if id_user:
        list_ = func_mem(id_user)
        if list_:
            return list_, True
        return await fun_db(id_user, cur_), False
    else:
        list_ = func_mem()
        if list_:
            return list_, True
        return await fun_db(cur_), False


async def get_list_chats_user(id_user,
                              cur,
                              cache):
    select = '''
        SELECT ref_id_chat
        FROM user_to_chat
        WHERE ref_id_user = {}
    '''.format(id_user)
    list_c = [r[0] for r in await cur.fetch(select)]
    await set_list_chat_to_user(id_user,
                                list_c,
                                cache)
    return list_c


async def get_count_new_message_user_in_chat(id_user,
                                             id_chat,
                                             cur,
                                             cache):
    data = await get_count_messages(id_user,
                                    id_chat,
                                    cache)
    if data:
        return data
    select = '''
        SELECT COUNT(ref_id_chat) 
        FROM chat_event_user
        WHERE ref_id_chat = {}
        AND ref_id_user = {}
    '''.format(id_chat, id_user)
    row = await cur.fetchrow(select)
    if row:
        await set_count_messages(id_user,
                                 id_chat,
                                 row[0],
                                 cache)
        return row[0]
    await set_count_messages(id_user,
                             id_chat,
                             0,
                             cache)
    return 0


async def db_get_chats_user_with_messages(id_user,
                                          page,
                                          pool,
                                          cache):
    async with pool.acquire() as conn:

        list_chat = await get_list_chats_user(id_user,
                                              conn,
                                              cache)
        list_chat = set(list_chat)
        list_chat = (list(map(str, list_chat)))
        select = '''
            SELECT m.id_message,m.text_message,  m.date_create,m.id_sender,m.name_sender,m.id_type_message,m.id_chat, m.title_chat, m.is_chat, m.path_to_img, m.is_get_notification
            FROM get_last_message_dialoge(ARRAY [{}],{}) m
            ORDER BY m.date_create DESC, m.id_chat ASC LIMIT {} OFFSET {};
        '''.format(','.join(list_chat), id_user, LIMIT_DIALOGS_ON_PAGE, page * LIMIT_DIALOGS_ON_PAGE)
        data_res = []
        try:
            for r in await conn.fetch(select):
                count = await get_count_new_message_user_in_chat(id_user,
                                                                 r[6],
                                                                 conn,
                                                                 cache)
                tmp = {
                    'id_message': r[0],
                    'text_message': r[1],
                    'created_on': str(r[2]),
                    'id_sender': r[3],
                    'name_sender': r[4],
                    'id_type_message': r[5],
                    'id_chat': r[6],
                    'title_chat': r[7],
                    'is_chat': r[8],
                    'path_img': r[9],
                    'get_notification': r[10],
                    'count_new':  count
                }
                data_res.append(tmp)
        except:
            pass
        return {
            'code': 1,
            'message': good_get_dialogs,
            'data': {
                'dialogs': data_res
            }
        }


async def db_get_messages_chat(id_user,
                               page,
                               id_chat,
                               pool,
                               cache):
    async def tmp(conn_):
        select = '''
                    SELECT id_message, created_on,text_message, ref_id_message_type, ref_id_sender
                    FROM messages
                    WHERE ref_id_chat = {}
                    ORDER BY created_on DESC LIMIT  {}  OFFSET {}
                '''.format(id_chat,
                           LIMIT_MESSAGES_ON_PAGE,
                           page * LIMIT_MESSAGES_ON_PAGE)

        answer = []
        users = {}
        for r in await conn_.fetch(select):
            if users.get(r[4], False):
                tmp_name = users[r[4]]['full_name']
                tmp_path_img = users[r[4]]['path_img']
            else:
                users[r[4]] = await db_get_user(id_user,
                                                conn_,
                                                cache)
                tmp_name = users[r[4]]['full_name']
                tmp_path_img = users[r[4]]['path_img']
            answer.append({
                'id_message': r[0],
                'created_on': str(r[1]),
                'text_message': r[2],
                'type_message': r[3],
                'ref_id_sender': r[4],
                'name_sender': tmp_name,
                'path_img': tmp_path_img
            })

        return {
            'code': 1,
            'message': good_get_messages,
            'data': {
                'messages': answer

            }
        }

    async with pool.acquire() as conn:
        res, is_mem = await get_l(conn,
                                  get_chats_user,
                                  get_list_chats_user,
                                  id_user)
        list_chat = set(res)
        if len(list_chat & {id_chat}) > 0:
            data = await tmp(conn)
            return data
        else:
            if is_mem:
                res = await get_list_chats_user(id_user,
                                                conn,
                                                cache)
                list_chat = set(res)
                if len(list_chat & {id_chat}) > 0:
                    data = await tmp(conn)
                    return data

        return {
            'code': 1,
            'message': error_permision,
            'data': {}
        }


async def db_remove_users(id_chat,
                          id_user,
                          list_for_remove,
                          pool,
                          cache):
    async with pool.acquire() as conn:
        data = await db_get_data_chat(id_chat,
                                      id_user,
                                      pool,
                                      cache)
        if data['message']['code'] > 0:
            data = data['data']['chat_info']
        else:
            select = '''
                SELECT is_main_chat, is_chat
                FROM chat
                WHERE id_chat = {}
            '''.format(id_chat)
            row = await conn.fetchrow(select)
            data = {
                'is_main_chat': row[0],
                'is_chat': row[1],
                'id_chat': id_chat
            }
        list_chat = []
        if data['is_main_chat']:
            select = '''
                SELECT id_chat
                FROM chat
                WHERE ref_main_chat = {}
            '''.format(id_chat)
            list_chat = [str(r[0]) for r in await conn.fetch(select)]
        list_chat.append(str(id_chat))

        select = '''
            DELETE 
            FROM user_to_chat WHERE ref_id_user in ({}) AND ref_id_chat in ({})
        '''.format(','.join(list_for_remove), ','.join(list_chat))

        await conn.execute(select)
        for l in list_chat:
            await set_information_chat(l,
                                       None,
                                       cache)
            await remove_users_from_chat(l,
                                         list_for_remove,
                                         cache)
            for u in list_for_remove:
                await remove_chat_on_user(l,
                                          u,
                                          cache)
                await remove_user_from_chat(l,
                                            u,
                                            cache)

        return {
            'code': 1,
            'message': good_remove_user,
            'data': {}
        }


async def is_user_exist_in_chat(id_chat,
                                id_user,
                                conn):
    select = '''
        SELECT ref_id_user
        FROM user_to_chat
        WHERE ref_id_user = {}
        AND ref_id_chat = {}
    '''.format(id_user, id_chat)
    res = await conn.fetchrow(select)
    return True if res else False


async def db_add_permission(id_user,
                            id_chat,
                            is_add,
                            pool,
                            cache):
    async with pool.acquire() as conn:
        if await is_user_exist_in_chat(id_chat,
                                       id_user,
                                       conn):
            if is_add:
                insert_permission = '''
                    INSERT INTO chats_admin (ref_id_user, ref_id_chat, is_root_admin) 
                    VALUES ({},{},FALSE );
                '''.format(id_user, id_chat)
                try:
                        await conn.execute(insert_permission)
                        is_good = True
                except:
                    is_good = False
                if is_good:
                    return {
                        'code': 1,
                        'message': good_set_admin,
                        'data': {}
                    }
                return {
                    'code': 1,
                    'message': error_set_admin,
                    'data': {}
                }
            else:
                remove_permission = '''
                    DELETE FROM chats_admin WHERE ref_id_chat = {} AND ref_id_user = {} 
                '''.format(id_chat, id_user)
                try:
                    await conn.execute(remove_permission)
                    is_good = True
                except:
                    is_good = False
            if is_good:
                await set_information_chat(id_chat,
                                           None,
                                           cache)
                return {
                    'code': 1,
                    'message': good_rem_permission,
                    'data': {}
                }
            return {
                'code': 1,
                'message': error_rem_permission,
                'data': {}
            }
        return {
            'code': 1,
            'message': error_update_permission,
            'data': {}
        }








