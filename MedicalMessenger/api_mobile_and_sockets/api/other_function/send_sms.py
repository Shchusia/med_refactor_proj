from zeep import Client
from MedicalMessenger.config_app import login_turbo_sms, password_turbo_sms
import traceback


def send_sms(number, text_sms_message,title_sms):
    try:
        client = Client('http://turbosms.in.ua/api/wsdl.html')
        # print(client)
        t = client.service.Auth(login_turbo_sms,password_turbo_sms)
        # print(t)
        t = client.service.SendSMS(title_sms, number, text_sms_message)
        # print(t)

        if t[0] == "Сообщения успешно отправлены":
            print(t[1])
            return t[1]
        else:
            return False
    except:
        traceback.print_exc()
        return False


# send_sms('+380979491074', 864297)