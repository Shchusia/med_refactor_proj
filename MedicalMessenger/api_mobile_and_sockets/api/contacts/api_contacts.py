from aiohttp import web
from MedicalMessenger.api_mobile_and_sockets.urls.api import api_get_list_dialogs,\
    api_get_list_messages,\
    api_remove_user_from_chat,\
    api_add_permission_admin_for_chat
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data
from MedicalMessenger.config_app import START_CODE_FOR_ERROR
from MedicalMessenger.api_mobile_and_sockets.answer_server import arguments_error, \
    error_unknown,\
    not_valid_token,\
    good_get_contacts
from MedicalMessenger.api_mobile_and_sockets.api.concrete_chat.db_concrete_chat import db_get_chats_user_with_messages
from MedicalMessenger.api_mobile_and_sockets.api.redis.redb import check_valid_data_multi
import traceback
from MedicalMessenger.api_mobile_and_sockets.api.contacts.db_contacts import db_get_contacts_for_user


async def get_contacts_for_user(request):
    try:
        pool = request.app['pool']  # пул connect to db
        r = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     r)):
            try:
                page = request.rel_url.query['page']
            except KeyError:
                page = 0
            try:
                limit = request.rel_url.query['limit']
            except KeyError:
                limit = 20

            return web.json_response(await db_get_contacts_for_user(data['id_user'],
                                                                    pool,
                                                                    page,
                                                                    limit))
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        },
            status=START_CODE_FOR_ERROR-not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_unknown['code'])

    pass

