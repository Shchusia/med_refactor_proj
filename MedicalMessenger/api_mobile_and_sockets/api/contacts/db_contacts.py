from MedicalMessenger.api_mobile_and_sockets.answer_server import good_get_contacts


async def db_get_contacts_for_user(id_user,
                                   pool,
                                   page=0,
                                   limit=20,
                                   ):
    async with pool.acquire() as conn:
        select = '''
            SELECT DISTINCT (u.id_user), u.full_name, u.phone, u.path_img, utc.is_ban
            FROM user_to_chat utc, users u
            WHERE utc.ref_id_chat in (
              SELECT uc.ref_id_chat
                FROM chat c, user_to_chat uc
                  WHERE uc.ref_id_chat = c.id_chat
                  AND uc.ref_id_user = {}
                  AND (c.is_chat = TRUE
                  AND c.is_main_chat = TRUE)
                  OR (c.is_chat = FALSE )
                  )
            AND utc.ref_id_user = u.id_user
            AND u.id_user != {}
            ORDER BY u.full_name limit {} offset {}
      '''.format(id_user, id_user,
                 int(limit),
                 int(limit) * int(page))
        # print(select)
        res = [{
            'id_user': r[0],
            'full_name': r[1],
            'phone': r[2],
            'path_img': r[3],
            'is_ban': r[4]
        } for r in await conn.fetch(select)]

        return {
            'code': 1,
            'message': good_get_contacts,
            'data': {
                'contacts': res
            }
        }
