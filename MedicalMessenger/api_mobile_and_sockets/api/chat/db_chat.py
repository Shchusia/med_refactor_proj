import time
import traceback
from datetime import datetime, timedelta

from MedicalMessenger.api_mobile_and_sockets.answer_server import good_get_chat, \
    error_not_your_chat_for_view, \
    good_entry_to_chat, \
    error_not_entry_to_chat_ald_code, \
    error_not_entry_you_in_chat, \
    good_create_dialog, \
    error_chat_exist_with_user, \
    default_name_dialog, \
    error_not_connect_to_chat, \
    error_entry_chat_exist_in_chat, \
    error_not_create_chat_by_not_in_one_chat, \
    good_generate_code, \
    good_get_types_chat
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat import set_information_chat, \
    get_information_chat, \
    set_primary_chat_partner, \
    set_type_sub_chat, \
    set_chat_user, \
    set_to_chats, \
    set_list_chat, \
    set_list_chat_to_user, \
    get_chats_user, \
    get_chats
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat_db import add_new_chat_to_user, \
    add_user_to_chat
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_chat_db import get_count_messages
from MedicalMessenger.api_mobile_and_sockets.api.memcache.mem_user import set_information_user, \
    get_information_user
from MedicalMessenger.api_mobile_and_sockets.api.redis.redis_chat import set_timestamp_last_update_chat_code
from MedicalMessenger.api_mobile_and_sockets.celery_med.celery_update_code import update_code
from MedicalMessenger.api_mobile_and_sockets.helpers.generators import generate_code
from MedicalMessenger.api_mobile_and_sockets.socket.memdb.mem_socket import set_users_to_chat
from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_chat import add_event_user
from MedicalMessenger.config_app import role_client, role_partner, \
    path_to_default_photo_profile
from MedicalMessenger.config_app import text_congratulation_user, \
    path_to_photo_bot_admin, \
    name_bot_admin, \
    name_chat_admin


async def db_get_user(id_user,
                      conn,
                      cache):
    '''
    получение данных пользователя
    :param id_user: айди пользователя
    :param conn: объект коннекта к базе
    :param cache: объект memcache
    :return: пользователь
    '''

    user = await get_information_user(id_user,
                                      cache)
    if user is None:
        select = '''
            SELECT u.id_user, u.phone, u.path_img, u.full_name, u.ref_id_role
            FROM users u
            WHERE u.id_user = {}
        '''.format(id_user)

        row = await conn.fetchrow(select)
        user = {
            'id_user': row[0],
            'phone': row[1],
            'full_name': row[3],

        }
        select = '''
            SELECT  c.city_title, r.region_
            FROM users u, city c, region r
            WHERE u.id_user = {}
            AND u.ref_id_city = c.id_city
            AND c.ref_id_region = r.id_region
        '''.format(row[0])
        row_c = await conn.fetchrow(select)
        if row_c:
            user['city_title'] = row_c[0]
            user['region'] = row_c[1]
        else:
            user['city_title'] = ''
            user['region'] = ''

        if row[4] == 3:
            user['role'] = role_client
            select = '''
                SELECT specialisation_
                FROM users, specialisation
                WHERE ref_id_specialisation = specialisation.id_specialisation
                AND id_user = {}
            '''.format(id_user)

            try:
                user['specialisation'] = (await conn.fetchrow(select))[0]
            except:
                user['specialisation'] = 0
        else:
            if row[4] == 2:
                user['role'] = role_partner
            else:
                user['role'] = role_partner
            user['specialisation'] = role_partner
        if row[2]:
            user['path_img'] = row[2]
        else:
            user['path_img'] = path_to_default_photo_profile
        await set_information_user(id_user,
                                   user,
                                   cache)
    return user


async def db_create_chat(id_partner,
                         title,
                         description,
                         path_to_img,
                         code_for_entry,
                         ttl_code,
                         is_main_chat,
                         pool,
                         cache,
                         redis_chat,
                         id_main_chat=None,
                         type_sub_chat=None):
    async with pool.acquire() as conn:
        timestamp = int(time.time())
        while True:
            try:
                if is_main_chat:
                    select = '''
                        INSERT INTO chat (id_chat, title, description, code_for_join, path_img, is_main_chat,  created, is_chat, ttl_code, ref_id_creater,date_last_update_code) 
                        VALUES (DEFAULT, '{}', '{}', '{}', '{}', TRUE, DEFAULT, TRUE, {},{}, {}) returning id_chat
                    '''.format(title, description, code_for_entry, path_to_img, ttl_code, id_partner, timestamp)
                else:
                    select = '''
                        INSERT INTO chat (id_chat, title, description, path_img, is_main_chat, ref_main_chat, ref_type_sub_chat, created, is_chat, ref_id_creater) 
                        VALUES (          DEFAULT, '{}',   '{}',          '{}', FALSE, {}, {}, DEFAULT, TRUE, {} ) returning id_chat
                    '''.format(title, description, path_to_img, id_main_chat, type_sub_chat, id_partner)

                row = await conn.fetchrow(select)
                break
            except:
                await conn.rollback()
                code_for_entry = generate_code()

        select = '''
            INSERT INTO user_to_chat(ref_id_user, ref_id_chat) VALUES ({}, {});
            INSERT INTO chats_admin (ref_id_user, ref_id_chat, is_root_admin) VALUES ({},{},TRUE);
        '''.format(id_partner, row[0],
                   id_partner, row[0])
        await conn.execute(select)
        if is_main_chat:
            await set_primary_chat_partner(id_partner,
                                           row[0],
                                           cache)
        if not is_main_chat:
            await set_information_chat(id_main_chat,
                                       None,
                                       cache)
        await add_user_to_chat(row[0],
                               id_partner,
                               cache)
        await set_chat_user(id_partner,
                            row[0],
                            cache)
        await set_to_chats(row[0], cache)
        await set_timestamp_last_update_chat_code(row[0], timestamp, redis_chat)
        tomorrow = datetime.utcnow() + timedelta(seconds=int(ttl_code))
        update_code.apply_async((row[0], timestamp, ttl_code), eta=tomorrow)
        data = await get_chat_data(row[0],
                                   id_partner,
                                   cache,
                                   pool)
        return data


async def get_data_chat_main(id_chat,
                             id_user,
                             conn,
                             cache):

    select = '''
        SELECT id_chat, title, description, path_img, code_for_join,is_main_chat, created, is_chat, ttl_code,ref_id_creater
        FROM chat, user_to_chat
        WHERE id_chat = {}
        AND id_chat = ref_id_chat
        AND ref_id_user  = {}
    '''.format(id_chat,
               id_user)
    row = await conn.fetchrow(select)
    data = {
        'id_chat': row[0],
        'title_chat': row[1],
        'description': row[2],
        'path_to_img': row[3],
        'code_for_join': row[4],
        'is_main_chat': row[5],
        'created_on': str(row[6]),
        'is_chat': row[7],
        'ttl_code': row[8],
        'creator': await db_get_user(row[9],
                                     conn,
                                     cache),
        'other_information': '',
        'users_in_chat': (await get_users_chat(row[0], conn))
    }
    return data


async def get_chat_data(id_chat,
                        id_user,
                        cache,
                        pool):
    data = await get_information_chat(id_chat,
                                      cache)
    if data is None:
        async with pool.acquire() as conn:
            select = '''
                    SELECT id_chat, title, description, path_img, code_for_join,is_main_chat, created, is_chat, ttl_code,ref_id_creater
                    FROM chat, user_to_chat
                    WHERE id_chat = {}
                    AND id_chat = ref_id_chat
                    AND ref_id_user  = {}
                '''.format(id_chat,
                           id_user)
            row = await conn.fetchrow(select)
            if row:
                data = {
                    'id_chat': row[0],
                    'title_chat': row[1],
                    'description': row[2],
                    'path_to_img': row[3],
                    'code_for_join': row[4],
                    'is_main_chat': row[5],
                    'created_on': str(row[6]),
                    'is_chat': row[7],
                    'ttl_code': row[8],
                    'creator': await db_get_user(row[9],
                                                 conn,
                                                 cache),
                    'other_information': None,
                    'users_in_chat': (await get_users_chat(row[0],
                                                           conn))
                }
                if not row[5]:
                    select = '''
                            SELECT id_chat, ref_main_chat, ref_type_sub_chat, type_chat
                            FROM chat, type_sub_chat
                            WHERE id_chat = {}
                            AND ref_type_sub_chat = type_sub_chat.id_type_sub_chat
                        '''.format(id_chat)
                    row = await conn.fetchrow(select)
                    other_data = {
                        'main_chat': await get_data_chat_main(row[0],
                                                              id_user,
                                                              conn,
                                                              cache),
                        'id_type_sub_chat': row[2],
                        'type_sub_chat': row[3]
                    }
                    data['other_information'] = other_data
                else:
                    select = '''
                        select id_chat, title, description, path_img, type_chat
                        FROM chat, type_sub_chat
                        where ref_main_chat  = {}
                        AND ref_type_sub_chat = type_sub_chat.id_type_sub_chat
                        ORDER BY created DESC
                    '''.format(id_chat)
                    sub_chats = [
                        {
                            'id_chat': r[0],
                            'title_chat': r[1],
                            'description': r[2],
                            'path_to_img': r[3],
                            'type_sub_cha': r[4]

                        } for r in await  conn.fetch(select)
                    ]
                    data['sub_chats'] = sub_chats
                await set_information_chat(id_chat,
                                           data,
                                           cache)

                return {
                    'code': 1,
                    'message': good_get_chat,
                    'data': {
                        'chat_info': data
                    }
                }
            return {
                'code': 1,
                'message': error_not_your_chat_for_view,
                'data': {}
            }
    return {
        'code': 1,
        'message': good_get_chat,
        'data': {
            'chat_info': data
        }
    }


async def db_get_data_chat(id_chat,
                           id_user,
                           pool,
                           cache):
    data = await get_chat_data(id_chat,
                               id_user,
                               cache,
                               pool)

    return data


async def is_valid_id_chat_primary(id_partner,
                                   id_chat,
                                   pool,
                                   cache):

    async def tmp(*args_,
                  **kwargs):

        id_partner_ = args_[0]
        id_chat_ = args_[1]
        async with pool.acquire() as conn:

            select = '''
                    SELECT id_chat
                    FROM chat
                    WHERE id_chat = {}
                    AND ref_id_creater = {}
                    AND is_chat = TRUE 
                    AND is_main_chat = TRUE 
                '''.format(id_chat_,
                           id_partner_)
            # print(select)
            row = await conn.fetchrow(select)
            if row:
                if int(row[0]) == int(id_chat_):
                    await set_primary_chat_partner(id_partner_,
                                                   id_chat_,
                                                   cache)
                    return True
            return False

    # res = is_exist_primary_chat_partner(id_partner,
    #                                    id_chat)
    # if res:
    #     return True
    return await tmp(id_partner,
                     id_chat)


async def is_valid_type_sub_chat(id_type, pool, cache):

    async def tmp(*args_,
                  **kwargs):
        id_type_ = args_[0]
        async with pool.acquire() as conn:

            select = '''
                    SELECT id_type_sub_chat
                    FROM type_sub_chat
                    WHERE id_type_sub_chat = {}
                '''.format(id_type_)
            row = await conn.fetchrow(select)
            # print(row)

            if row:
                # print(row)
                if int(row[0]) == int(id_type_):
                    # print(1234)
                    await set_type_sub_chat(id_type_, cache)
                    return True
            return False

    # res = is_exist_type_sub_chat(id_type)

    # if res is None:
    return await tmp(id_type)
    # return res


async def entry_user_to_chat(id_user,
                             code,
                             pool,
                             cache):
    async with pool.acquire() as conn:
        select = '''
            SELECT id_chat
            FROM chat
            WHERE code_for_join = '{}'
        '''.format(code)
        row = await conn.fetchrow(select)
        if row:
            try:
                select = '''
                        INSERT INTO user_to_chat (ref_id_user, ref_id_chat) VALUES ({}, {})
                '''.format(id_user, row[0])

                await conn.execute(select)
                await set_chat_user(id_user, row[0], cache)
                await add_new_chat_to_user(id_user, row[0], cache)
                await add_user_to_chat(row[0], id_user, cache)
                await set_users_to_chat(row[0], None, cache)
                await set_chat_user(id_user, row[0], cache)
                await set_information_chat(row[0], None, cache)
                data = {
                    'code': 1,
                    'message': good_entry_to_chat,
                    'data': (await get_chat_data(row[0],
                                                 id_user,
                                                 cache,
                                                 pool))['data']
                }

                return data
            except:
                traceback.print_exc()
                return {
                    'code': 1,
                    'message': error_not_entry_you_in_chat,
                    'data': {}
                }
        return {
            'code': 1,
            'message': error_not_entry_to_chat_ald_code,
            'data': {}
        }


async def get_list_chats_user(id_user,
                              conn,
                              cache):
    select = '''
        SELECT ref_id_chat
        FROM user_to_chat
        WHERE ref_id_user = {}
    '''.format(id_user)

    list_c = [r[0] for r in await conn.fetch(select)]
    await set_list_chat_to_user(id_user,
                                list_c,
                                cache)
    return list_c


async def db_get_list_chat(conn,
                           cache):
    select = '''
        SELECT id_chat
        FROM chat
        WHERE is_chat = TRUE 
    '''
    list_c = [r[0] for r in await conn.fetch(select)]
    await set_list_chat(list_c,
                        cache)
    return list_c


async def db_create_dialog(id_user1,
                           id_user2,
                           pool,
                           cache):
    async def get_l(cur_,
                    func_mem,
                    fun_db,
                    id_user=None):
        if id_user:
            list_ = func_mem(id_user)
            if list_:
                return list_
            return await fun_db(id_user, cur_)
        else:
            list_ = func_mem()
            if list_:
                return list_
            return await fun_db(cur_)

    async with pool.acquire() as conn:

        list_chat_user1 = set(await get_l(conn, get_chats_user, get_list_chats_user, id_user1))
        list_chat_user2 = set(await get_l(conn, get_chats_user, get_list_chats_user, id_user2))
        list_chats = set(await get_l(conn, get_chats, db_get_list_chat))
        answer = list((set(list_chat_user1) & set(list_chat_user2)) - set(list_chats))
        if len(answer) > 0:

            return {
                'code': 1,
                'message': error_chat_exist_with_user,
                'data': (await get_chat_data(answer[0],
                                             id_user1,
                                             cache,
                                             pool))['data']
            }
        else:
            if len(list(set(list_chat_user1) & set(list_chat_user2))) > 0:
                select = '''
                    INSERT INTO chat (id_chat, title, path_img, is_main_chat, is_chat, ref_id_creater, created) 
                    VALUES (DEFAULT, '{}','{}', TRUE , FALSE, {},DEFAULT ) returning id_chat
                '''.format(default_name_dialog,
                           path_to_default_photo_profile,
                           id_user1)

                cur = await conn.fetchrow(select)
                id_chat = cur[0]
                if id_user1 == id_user2:
                    select = '''
                                    INSERT INTO user_to_chat (ref_id_user, ref_id_chat) VALUES ({},{}),({},{});
                                '''.format(id_user1, id_chat)
                else:
                    select = '''
                        INSERT INTO user_to_chat (ref_id_user, ref_id_chat) VALUES ({},{}),({},{});
                    '''.format(id_user1, id_chat,
                               id_user2, id_chat)

                await conn.execute(select)
                await add_new_chat_to_user(id_user1, id_chat, cache)
                await add_new_chat_to_user(id_user2, id_chat, cache)
                await set_chat_user(id_user1, id_chat, cache)
                await set_chat_user(id_user2, id_chat, cache)
                await add_user_to_chat(id_chat, id_user1, cache)
                await add_user_to_chat(id_chat, id_user2, cache)
                data = {
                    'code': 1,
                    'message': good_create_dialog,
                    'data': (await get_chat_data(id_chat,
                                                 id_user1,
                                                 cache,
                                                 pool))['data']
                }

                return data

            return {
                'code': 1,
                'message': error_not_create_chat_by_not_in_one_chat,
                'data': {}
            }


async def connect_to_sub_chat(id_user,
                              id_chat,
                              pool,
                              cache):
    select = '''
        SELECT id_chat
        FROM chat c
        WHERE is_main_chat = FALSE
        AND ref_main_chat in (
          SELECT id_chat
          FROM user_to_chat, chat
          WHERE ref_id_user = {}
          AND ref_id_chat = id_chat
          AND is_chat = TRUE
          AND is_main_chat = TRUE)
        AND id_chat = {}
    '''.format(id_user, id_chat)
    async with pool.acquire() as conn:
        row = await conn.fetchrow(select)
        if row:
            try:
                select = '''
                    INSERT INTO user_to_chat (ref_id_user, ref_id_chat) VALUES ({},{})
                '''.format(id_user, id_chat)

                await conn.execute(select)
                data = {
                    'code': 1,
                    'message': good_entry_to_chat,
                    'data': (await get_chat_data(row[0],
                                                 id_user,
                                                 cache,
                                                 pool))['data']
                }
                return data
            except:
                data = {
                    'code': 1,
                    'message': error_entry_chat_exist_in_chat,
                    'data': (await get_chat_data(id_chat,
                                                 id_user,
                                                 cache,
                                                 pool))['data']
                }
                return data
    return {
        'code': 1,
        'message': error_not_connect_to_chat,
        'data': {}
    }


async def is_sub_chat(id_chat,
                      id_user,
                      pool):
    async with pool.acquire() as conn:
        select = '''
            SELECT id_chat
            FROM chat
            WHERE id_chat = {}
            AND ref_id_creater = {}
            AND is_chat = TRUE 
            AND is_main_chat = FALSE 
        '''.format(id_chat, id_user)
        row = await conn.fetchrow(select)
        if row:
            if (row[0]) == int(id_chat):
                return True
        return False


#
async def db_update_information_chat(select,
                                     id_chat,
                                     id_user,
                                     pool,
                                     cache):
    async with pool.acquire() as conn:
        await conn.execute(select)
        await set_information_chat(id_chat, None, cache)
        return await get_chat_data(id_chat,
                                   id_user,
                                   cache,
                                   pool)


async def update_inf_redis(id_chat,
                           timestamp,
                           ttl_code,
                           redis_chat):
    await set_timestamp_last_update_chat_code(id_chat,
                                              timestamp,
                                              redis_chat)
    tomorrow = datetime.utcnow() + timedelta(seconds=ttl_code)
    update_code.apply_async((id_chat,
                             timestamp,
                             ttl_code),
                            eta=tomorrow)


async def is_chat_user(id_chat,
                       id_user,
                       pool):
    async with pool.acquire() as conn:

        select = '''
            SELECT id_chat
            FROM chat
            WHERE id_chat = {}
            AND ref_id_creater = {}
            AND is_chat = TRUE 
            AND is_main_chat = TRUE 
        '''.format(id_chat, id_user)
        row = await conn.fetchrow(select)
        if row:
            if int(row[0]) == int(id_chat):
                return True
        return False


async def is_admin_chat(id_chat,
                        id_user,
                        pool):
    async with pool.acquire() as conn:
        select = '''
            SELECT ref_id_user
            FROM chats_admin
            WHERE ref_id_chat = {}
            AND ref_id_user = {}
        '''.format(id_chat, id_user)
        row = await conn.fetchrow(select)
        if row:
            if int(row[0]) == int(id_user):
                return True
    return False


async def is_admin_chat_global_for_upload_photo(id_user,
                                                pool):
    async with pool.acquire() as conn:
        select = '''
            SELECT ref_id_user
            FROM chats_admin
            WHERE ref_id_user = {}
        '''.format(id_user)

        row = await conn.fetchrow(select)
        if row:
            if int(row[0]) == int(id_user):
                return True
    return False


async def db_auto_generate_code(id_chat,
                                timestamp,
                                pool,
                                cache,
                                redis_chat):
    async with pool.acquire() as conn:
        new_code = generate_code()
        select = '''
                SELECT id_chat
                FROM chat
                WHERE code_for_join = '{}'
            '''
        while True:
            row = await conn.fetchrow(select.format(new_code))
            if row:
                pass
            else:
                break
            new_code = generate_code()
        select = '''
            UPDATE chat
            SET code_for_join = '{}',
            date_last_update_code = {}
            WHERE id_chat = {}
        '''.format(new_code, timestamp, id_chat)

        await conn.execute(select)

        data = await get_information_chat(id_chat, cache)
        if data:
            ttl_code = data['ttl_code']
            await update_inf_redis(id_chat,
                                   timestamp,
                                   ttl_code,
                                   redis_chat)
        else:
            select = '''
                SELECT ttl_code
                FROM chat
                WHERE id_chat = {}
            '''.format(id_chat)
            row = await conn.fetchrow(select)
            if row:
                await update_inf_redis(id_chat,
                                       timestamp,
                                       row[0],
                                       redis_chat)
            else:
                await update_inf_redis(id_chat,
                                       timestamp,
                                       24 * 60 * 60,
                                       redis_chat)

        return {
            'code': 1,
            'message': good_generate_code,
            'data': {
                'new_code': new_code
            }
        }


async def is_exist_code(code, pool):
    async with pool.acquire() as conn:
        select = '''
            SELECT id_chat
            FROM chat
            WHERE code_for_join = '{}'
        '''.format(code)
        row = await conn.fetchrow(select)

        if row:
            return True
        return False


async def is_chat_partner(id_partner,
                          id_chat,
                          pool):
    async with pool.acquire() as conn:
        select = '''
            SELECT ref_id_creater
            FROM chat
            WHERE id_chat = {}
            AND is_chat = TRUE 
        '''.format(id_chat)
        row = await conn.fetchrow(select)
        if row:
            if int(row[0]) == int(id_partner):
                return True
        return False


async def ban_user(id_chat,
                   id_users,
                   is_ban,
                   pool,
                   cache):
    async with pool.acquire() as conn:
        id_users_for_bun = list(map(str, id_users))
        update_status = '''
            UPDATE user_to_chat
            SET is_ban = {}
            WHERE ref_id_chat = {}
            AND ref_id_user IN ({})
            AND ref_id_user != (SELECT ref_id_creater 
                                FROM chat
                                WHERE id_chat = {})
            '''.format(is_ban,
                       id_chat,
                       ",".join(id_users_for_bun),
                       id_chat)

        await conn.execute(update_status)

        await set_users_to_chat(id_chat, None, cache)
        await set_information_chat(id_chat, None, cache)
        return True


async def get_users_chat(id_chat,
                         conn):
    select = '''
        SELECT us.id_user, us.full_name, us.phone, us.path_ing, us.is_ban, us.ref_id_specialisation, us.type_user
        FROM get_users_chat({}) us
        ORDER BY us.type_user
    '''.format(id_chat)
    res = [
        {
            'id_user': r[0],
            'full_name': r[1],
            'phone': r[2],
            'path_img': r[3],
            'is_ban': r[4],
            'ref_id_specialisation': r[5],
            'type_user': r[6]
        } for r in await conn.fetch(select)
    ]
    return res


async def db_get_types_sub_chat(pool):
    async with pool.acquire() as conn:
        select = '''
            SELECT id_type_sub_chat, type_chat
            FROM type_sub_chat
        '''
        res = [{
            'id_type_sub_chat': r[0],
            'type_sub_chat': r[1]
        } for r in await conn.fetch(select)]

        return {
            'code': 1,
            'message': good_get_types_chat,
            'data': {
                'types': res
            }
        }


async def db_send_congratulation_message(id_user,
                                         full_name,
                                         pool,
                                         r_chat,
                                         cache):
    async with pool.acquire() as conn:
        select = '''
            INSERT INTO messages (id_message, ref_id_message_type, ref_id_sender, ref_id_chat, created_on, text_message) 
            VALUES (DEFAULT, 1, 0, 0, DEFAULT, '{}') returning id_message, created_on
        '''.format(text_congratulation_user.format(full_name))
        row = await conn.fetchrow(select)
        select = '''
            INSERT INTO view_message (ref_id_user, ref_id_message) VALUES ({}, {})
        '''.format(id_user, row[0])
        await add_event_user(id_user, 0, r_chat)
        await conn.execute(select)
        return {
            'id_message': row[0],
            'text_message': text_congratulation_user.format(full_name),
            'created_on': str(row[1]),
            'ref_id_sender': id_user,
            'name_sender': name_bot_admin,
            'id_type_message': 1,
            'id_chat': 0,
            'title_chat': name_chat_admin,
            'is_chat': True,
            'path_img': path_to_photo_bot_admin,
            'count_new': (await get_count_messages(id_user, 0, cache))
        }
