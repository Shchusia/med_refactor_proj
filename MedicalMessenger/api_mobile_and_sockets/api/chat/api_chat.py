from aiohttp import web
import traceback

from MedicalMessenger.config_app import START_CODE_FOR_ERROR,\
    MEDIA_FOLDER,\
    str_path_to_file,\
    domain_photo,\
    path_to_photo_chat,\
    role_partner,\
    role_admin_app,\
    path_to_default_photo_chat
import binascii
import os
import time
from MedicalMessenger.global_functions.compress_photo import comppress_photo
from MedicalMessenger.api_mobile_and_sockets.answer_server import arguments_error, \
    good_upload_file,\
    error_you_not_partner,\
    error_not_exist_sub_chat_type,\
    error_not_your_chat,\
    error_unknown,\
    error_not_create_chat_with_your_self,\
    error_permission_update_chat,\
    not_valid_token,\
    error_permission_for_bun,\
    good_ban_users,\
    error_permission_not_admin
from MedicalMessenger.api_mobile_and_sockets.helpers.validators import token_to_data
from MedicalMessenger.api_mobile_and_sockets.api.chat.db_chat import db_create_chat,\
    is_valid_id_chat_primary,\
    is_valid_type_sub_chat,\
    entry_user_to_chat,\
    connect_to_sub_chat,\
    db_create_dialog,\
    is_sub_chat,\
    is_exist_code, \
    db_update_information_chat,\
    update_inf_redis,\
    db_auto_generate_code,\
    ban_user as bu,\
    is_chat_partner,\
    db_get_data_chat, \
    is_admin_chat,\
    is_admin_chat_global_for_upload_photo,\
    db_get_types_sub_chat
from MedicalMessenger.api_mobile_and_sockets.helpers.generators import generate_code
from MedicalMessenger.api_mobile_and_sockets.api.redis.redb import check_valid_data_multi


MAX_FILE_SIZE = 1024 * 1024 + 1


async def create_chat(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis_chat = request.app['redis_chat']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):

            if data['role'] == (role_partner or role_admin_app):
                di = dict(await request.post())

                title_chat = di['title_chat']
                try:
                    description = di['description']
                except:
                    description = ''
                try:
                    path_to_img = di['path_to_img']
                except:
                    path_to_img = path_to_default_photo_chat
                try:
                    code = di['code_for_entry']
                except KeyError:
                    code = generate_code()
                try:
                    ttl_code = di['ttl_code']
                except KeyError:
                    ttl_code = 3600 * 24
                try:
                    is_main = False
                    primary_chat = di['id_chat_main']
                    if await is_valid_id_chat_primary(data['id_user'],
                                                      primary_chat,
                                                      pool,
                                                      cache):
                        type_sub_chat = di['type_sub_chat']
                        if await is_valid_type_sub_chat(type_sub_chat,
                                                        pool,
                                                        cache):
                            is_main = False
                        else:
                            return web.json_response({
                                'code': 1,
                                'message': error_not_exist_sub_chat_type,
                                'data': {}
                            }, status=START_CODE_FOR_ERROR - error_not_exist_sub_chat_type['code'])
                    else:
                        return web.json_response({
                            'code': 1,
                            'message': error_not_your_chat,
                            'data': {}
                        }, status=START_CODE_FOR_ERROR - error_not_your_chat['code'])

                except:
                    traceback.print_exc()
                    is_main = True
                if is_main:
                    data = await db_create_chat(data['id_user'],
                                                title_chat,
                                                description,
                                                path_to_img,
                                                code,
                                                ttl_code,
                                                is_main,
                                                pool,
                                                cache,
                                                redis_chat)
                else:
                    data = await db_create_chat(data['id_user'],
                                                title_chat,
                                                description,
                                                path_to_img,
                                                code,
                                                ttl_code,
                                                is_main,
                                                pool,
                                                cache,
                                                redis_chat,
                                                id_main_chat=primary_chat,
                                                type_sub_chat=type_sub_chat)
                if data['message']['code'] > 0:
                    return web.json_response(data)
                return web.json_response(data, status=START_CODE_FOR_ERROR - data['message']['code'])
            return web.json_response({
                'code': 1,
                'message': error_you_not_partner,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_you_not_partner['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def upload_photo_chat(request):
    try:
        pool = request.app['pool']  # пул connect to db
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            if await is_admin_chat_global_for_upload_photo(data['id_user'],
                                                           pool):
                di = await request.post()
                filename = di['file'].filename
                file = di['file'].file
                if not os.path.exists(MEDIA_FOLDER + str_path_to_file + path_to_photo_chat):
                    os.makedirs(MEDIA_FOLDER + str_path_to_file + path_to_photo_chat)
                # print(file)
                name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(filename.split('.')[-1])
                out = open(MEDIA_FOLDER + str_path_to_file + path_to_photo_chat + name, 'wb')
                while True:
                    file_bytes = file.read(MAX_FILE_SIZE)
                    out.write(file_bytes)
                    if len(file_bytes) < MAX_FILE_SIZE:
                        break
                out.close()
                comppress_photo(MEDIA_FOLDER + str_path_to_file + path_to_photo_chat + name)
                path = domain_photo + str_path_to_file + path_to_photo_chat + '.'.join(name.split('.')[:-1]) + '.jpeg'

                return web.json_response({
                        'code': 0,
                        'message': good_upload_file,
                        'data': {
                            'path_to_file': path
                        }
                })
            return web.json_response({
                'code': 1,
                'message': error_permission_not_admin,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_permission_not_admin['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
            # traceback.print_exc()
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])


async def entry_to_chat(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            di = dict(await request.post())
            code_for_entry = di['code_for_entry']
            res = await entry_user_to_chat(data['id_user'],
                                           code_for_entry,
                                           pool,
                                           cache)
            if res['message']['code'] > 0:
                return web.json_response(res)
            else:
                return web.json_response(res, status=START_CODE_FOR_ERROR - res['message']['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
            traceback.print_exc()
            return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def entry_to_sub_chat(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            di = dict(await request.post())
            id_chat = di['id_chat']
            res = await connect_to_sub_chat(data['id_user'],
                                            id_chat,
                                            pool,
                                            cache)
            if res['message']['code'] > 0:
                return web.json_response(res)
            else:
                return web.json_response(res, status=START_CODE_FOR_ERROR - res['message']['code'])
        return web.json_response({
                'code': -1,
                'message': not_valid_token,
                'data': {}
            }, status=START_CODE_FOR_ERROR - not_valid_token['code'])

    except KeyError:
        traceback.print_exc()
        return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def create_dialog(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            di = dict(await request.post())
            id_user = di['id_user']
            if int(data['id_user']) != int(id_user):
                res = await db_create_dialog(data['id_user'],
                                             id_user,
                                             pool,
                                             cache)
                if res['message']['code'] > 0:
                    return web.json_response(res)
                else:
                    return web.json_response(res,
                                             status=START_CODE_FOR_ERROR - res['message']['code'])
            return web.json_response({
                'code': 1,
                'message': error_not_create_chat_with_your_self,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_not_create_chat_with_your_self['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
            traceback.print_exc()
            return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def update_information_chat(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis_chat = request.app['redis_chat']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            is_good, data = token_to_data(request.headers['Token'])
            id_user = data['id_user']
            di = dict(await request.post())
            id_chat = di['id_chat']
            if await is_admin_chat(id_chat,
                                   id_user,
                                   pool):
                select = '''
                    UPDATE chat
                    SET {}
                    WHERE id_chat = {}       
                '''
                val = []

                timestamp = int(time.time())
                try:
                    new_name = di['new_title']
                    val.append("title = '{}'".format(new_name.replace("'", "''")))
                except:
                    pass
                try:
                    description = di['new_description']
                    val.append("description = '{}'".format(description.replace("'", "''")))
                except:
                    pass
                try:
                    path_img = di['path_to_img']
                    val.append("path_img = '{}'".format(path_img))
                except:
                    pass
                try:
                    new_type = int(di['id_type'])
                    if await is_sub_chat(id_chat, data['id_user'], pool):
                        if await is_valid_type_sub_chat(new_type,
                                                        pool,
                                                        cache):
                            val.append('ref_type_sub_chat = {}')
                except:
                    pass
                try:
                    new_ttl = int(di['new_ttl'])
                    val.append("ttl_code = {}".format(new_ttl))
                except:
                    pass
                try:
                    new_code = di['new_code']
                    while True:
                        if (await is_exist_code(new_code, pool)) is False:
                            break
                        new_code = generate_code()
                    val.append("code_for_join = '{}'".format(new_code))
                    val.append("date_last_update_code = {}".format(timestamp))
                    is_update_code = True
                except:
                    is_update_code = False
                data = await db_update_information_chat(select.format(','.join(val), id_chat),
                                                        id_chat,
                                                        id_user,
                                                        pool,
                                                        cache)
                if data['message']['code'] > 0:
                    if is_update_code:
                        await update_inf_redis(id_chat,
                                         timestamp,
                                         data['data']['chat_info']['ttl_code'],
                                         redis_chat)
                    return web.json_response(data)
                return web.json_response(data, status=START_CODE_FOR_ERROR - data['code'])
            return web.json_response({
                'code': 1,
                'message': error_permission_update_chat,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_permission_update_chat['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
            traceback.print_exc()
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            },status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
            return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def auto_generate_code(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis_chat = request.app['redis_chat']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            id_chat = request.headers['id_chat']
            is_good, data = token_to_data(request.headers['Token'])
            timestamp = int(time.time())

            if await is_admin_chat(id_chat,
                                   data['id_user'],
                                   pool):
                data = await db_auto_generate_code(id_chat,
                                                   timestamp,
                                                   pool,
                                                   cache,
                                                   redis_chat)
                if data['message']['code'] > 0:
                    return web.json_response(data)
                return web.json_response(data, status=START_CODE_FOR_ERROR - data['message']['code'])
            return web.json_response({
                'code': 1,
                'message': error_permission_update_chat,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_permission_update_chat['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])
    except KeyError:
            traceback.print_exc()
            return web.json_response({
                'code': 0,
                'message': arguments_error,
                'data': {}
            }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
            traceback.print_exc()
            return web.json_response({
                'code': 1,
                'message': error_unknown,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def ban_user(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            di = dict(await request.post())
            id_chat = di['id_chat']
            if (data['role'] == role_partner
                and await is_chat_partner(data['id_user'],
                                          id_chat,
                                          pool)) \
                    or (await is_admin_chat(id_chat,
                                            data['id_user'],
                                            pool)):
                is_ban = str(di['is_ban'])
                if is_ban.lower() == 'true':
                    is_ban = True
                else:
                    is_ban = False
                list_users = list(map(int, (await request.post()).getall('users')))
                await bu(id_chat,
                         list_users,
                         is_ban,
                         pool,
                         cache)
                return web.json_response({
                    'code': 1,
                    'message': good_ban_users,
                    'data': {}
                })
            return web.json_response({
                'code': 1,
                'message': error_permission_for_bun,
                'data': {}
            }, status=START_CODE_FOR_ERROR - error_permission_for_bun['code'])
        return web.json_response({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }, status=START_CODE_FOR_ERROR - not_valid_token['code'])

    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def chat_info(request):
    try:
        pool = request.app['pool']  # пул connect to db
        cache = request.app['mem']
        redis = request.app['redis']
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good and (await check_valid_data_multi(data['id_user'],
                                                     token,
                                                     data['role'],
                                                     redis)):
            id_chat = request.headers['id_chat']
            data = await db_get_data_chat(id_chat,
                                          data['id_user'],
                                          pool,
                                          cache)
            if data['message']['code'] > 0:
                return web.json_response(data)
            return web.json_response(data,
                                     status=START_CODE_FOR_ERROR-data['message']['code'])
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_unknown['code'])


async def get_types_sub_chat(request):
    pool = request.app['pool']  # пул connect to db
    try:
        return web.json_response(await db_get_types_sub_chat(pool))
    except KeyError:
        traceback.print_exc()
        return web.json_response({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }, status=START_CODE_FOR_ERROR - arguments_error['code'])
    except:
        traceback.print_exc()
        return web.json_response({
            'code': 1,
            'message': error_unknown,
            'data': {}
        }, status=START_CODE_FOR_ERROR - error_unknown['code'])


