
import datetime


async def set_phone(phone, r, ttl=300,):
    await r.set(phone, 'cool')
    await r.expire(phone, ttl)


async def check_phone(phone, r):
    try:
        is_exist = await r.get(phone)
        # print(is_exist)
        if is_exist is None:
            return True
        return False
    except:
        return True


async def get_time_(phone, r):
    try:
        return await r.ttl(phone)
    except:
        return 0


async def get_time_code(phone, rp):
    try:
        return await rp.ttl(phone)
    except:
        return 0


def get_time_for_next_day():
    d = datetime.datetime.today()
    try:

        next_day = datetime.datetime.today().replace(day=d.day + 1)
    except ValueError:

        try:
            next_day = datetime.datetime.today().replace(month=d.month + 1, day=1)
        except ValueError:
            next_day = datetime.datetime.today().replace(year=d.year + 1, month=1, day=1)
    new_day = datetime.datetime(year=next_day.year, month=next_day.month, day=next_day.day)

    tn = new_day.timestamp()
    cd = d.timestamp()
    return int(tn) - int(cd)


async def get_count_code(phone, rp):
    try:
        temp = await rp.get(phone)
        if temp:
            return int(temp)
        return 0
    except:
        return 0


async def add_count_code(phone, rp):
    try:
        temp = await rp.get(phone)
        if temp:
            c = int(temp) + 1
            await rp.set(phone, c)
            await rp.expire(phone, get_time_for_next_day())  # если надо ттл
        else:
            await rp.set(phone, 1)
            await rp.expire(phone, get_time_for_next_day())  # если надо ттл
    except:
        await rp.set(phone, 1)
        await rp.expire(phone, get_time_for_next_day())  # если надо ттл
