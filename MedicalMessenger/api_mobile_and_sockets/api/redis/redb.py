import traceback

ttl_phone_in_redis = 300


async def set_client_multi(id_user, session_key, role, r):
    await r.lpush('{}_{}'.format(role, id_user), session_key)


async def check_valid_data_multi(id_user, session_key, role, r):
    try:
        for li in list(await r.lrange('{}_{}'.format(role,id_user), 0, -1)):
            if li == session_key:
                return True
        return False
    except:
        traceback.print_exc()
    return False


async def sign_out_multi(id_user, session_key, role,r):
    try:
        await r.lrem('{}_{}'.format(role, id_user), 1, session_key)
    except:
        pass


async def remove_user(id_user, role,r):
    try:
        for li in list(await r.lrange('{}_{}'.format(role, id_user), 0, -1)):
            try:
                await r.delete(li)
            except:
                traceback.print_exc()
                pass
        await r.delete('{}_{}'.format(role, id_user))
    except:
        traceback.print_exc()
