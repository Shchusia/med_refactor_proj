import traceback


async def set_timestamp_last_update_chat_code(id_chat, timestamp, r):
    await r.set('chat_{}'.format(id_chat), timestamp)


async def get_timestamp_last_update_chat_code(id_chat, r):
    try:
        return int(await r.get('chat_{}'.format(id_chat)))
    except:
        return -1


async def set_ttl_last_update_chat_code(id_chat, ttl, r):
    await r.set('chat_{}_ttl'.format(id_chat), ttl)


async def get_ttl_last_update_chat_code(id_chat, r):
    try:
        return int(await r.get('chat_{}_ttl'.format(id_chat)))
    except:
        return -1
