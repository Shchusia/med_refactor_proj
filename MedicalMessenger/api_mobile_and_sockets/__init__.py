import aio_pika

import asyncio
import aiomcache
import aioredis
import asyncpg
from aiohttp import web

from MedicalMessenger.api_mobile_and_sockets.entities.entity_socket import MySio
from MedicalMessenger.config_app import mem_data
from MedicalMessenger.config_app import r_async_data, \
    r_sms_async_data, \
    r_time_async_data, \
    r_socket_async_data, \
    r_chat_async_data, \
    r_contacts_async_data
from MedicalMessenger.config_app import rabbit_mq_port
from MedicalMessenger.config_app import user, \
    database, \
    password, \
    host, \
    port
from aiocache import caches, SimpleMemoryCache, MemcachedCache
from celery import Celery
from MedicalMessenger.config_app import path_to_celery
from MedicalMessenger.config_app import secret_word_for_congratulation
from MedicalMessenger.api_mobile_and_sockets.socket.redb.socket_data import get_sid_user
from MedicalMessenger.api_mobile_and_sockets.urls.socket import socket_new_message
from MedicalMessenger.config_app import namespace
from MedicalMessenger.api_mobile_and_sockets.celery_med.celery_send_push import send_push_message
import traceback
from MedicalMessenger.api_mobile_and_sockets.urls.api import api_congratulation_user


async def init_app(loop):
    caches.set_config({
        'default': {
            'cache': "aiocache.SimpleMemoryCache",
            'serializer': {
                'class': "aiocache.serializers.StringSerializer"
            }
        },
        'memcached_alt': {
            'cache': "aiocache.MemcachedCache",
            'endpoint': mem_data['host'],
            'port': mem_data['port'],
            'timeout': 1,
            'serializer': {
                'class': "aiocache.serializers.PickleSerializer"
            },
            'plugins': [
                {'class': "aiocache.plugins.HitMissRatioPlugin"},
                {'class': "aiocache.plugins.TimingPlugin"}
            ]
        }
    })
    from MedicalMessenger.api_mobile_and_sockets.api import routes


    sio = MySio(async_mode='aiohttp')
    app = web.Application()
    sio.attach(app)

    # подключение к Postgresql
    pool = await asyncpg.create_pool(user=user,
                                     database=database,
                                     password=password,
                                     host=host,
                                     port=port)
    # подключение к Memcache
    mc = caches.get('memcached_alt')
    rabbit_connect = await aio_pika.connect_robust("amqp://guest:guest@127.0.0.1/",
                                                   loop=loop,
                                                   port=rabbit_mq_port)
    redis = await aioredis.create_redis_pool((r_async_data["host"],
                                              r_async_data["port"]),
                                             db=r_async_data["db"],
                                             encoding=r_async_data["encoding"])
    redis_sms = await aioredis.create_redis_pool((r_sms_async_data["host"],
                                                  r_sms_async_data["port"]),
                                                 db=r_sms_async_data["db"],
                                                 encoding=r_sms_async_data["encoding"])

    redis_time = await aioredis.create_redis_pool((r_time_async_data["host"],
                                                   r_time_async_data["port"]),
                                                  db=r_time_async_data["db"],
                                                  encoding=r_time_async_data["encoding"])

    redis_socket = await aioredis.create_redis_pool((r_socket_async_data["host"],
                                                     r_socket_async_data["port"]),
                                                    db=r_socket_async_data["db"],
                                                    encoding=r_socket_async_data["encoding"])

    redis_contacts = await aioredis.create_redis_pool((r_contacts_async_data["host"],
                                                       r_contacts_async_data["port"]),
                                                      db=r_contacts_async_data["db"],
                                                      encoding=r_contacts_async_data["encoding"])

    redis_chat = await aioredis.create_redis_pool((r_chat_async_data["host"],
                                                   r_chat_async_data["port"]),
                                                  db=r_chat_async_data["db"],
                                                  encoding=r_chat_async_data["encoding"])

    sio.pool = pool
    sio.mem = mc
    sio.rabbit_connect = rabbit_connect
    sio.redis_ = redis
    sio.redis_sms = redis_sms
    sio.redis_time = redis_time
    sio.redis_socket = redis_socket
    sio.redis_contacts = redis_contacts
    sio.redis_chat = redis_chat

    app['pool'] = pool
    app['mem'] = mc
    app['rabbit_connect'] = rabbit_connect
    app['redis'] = redis
    app['redis_sms'] = redis_sms
    app['redis_time'] = redis_time
    app['redis_socket'] = redis_socket
    app['redis_contacts'] = redis_contacts
    app['redis_chat'] = redis_chat

    app_celery = Celery('evenot', broker=path_to_celery)

    for route in routes:
        app.router.add_route(route[0],
                             route[1],
                             route[2],
                             name=route[3])
    return app, sio, app_celery


loop_ = asyncio.get_event_loop()
app, sio, app_celery = loop_.run_until_complete(init_app(loop_))

from MedicalMessenger.api_mobile_and_sockets.api.chat.db_chat import db_send_congratulation_message

async def congratulation_user(request):
    pool = request.app['pool']  # пул connect to db
    cache = request.app['mem']
    redis_chat = request.app['redis_chat']
    # print(request.headers['Host'].split(':')[0])
    # print(request.headers['Secret-Word'] == secret_word_for_congratulation)
    if (request.headers['Host'].split(':')[0] == '127.0.0.1'
        or request.headers['Host'].split(':')[0] == 'localhost')\
            and request.headers['Secret-Word'] == secret_word_for_congratulation:
        di = dict(await request.post())
        data = await db_send_congratulation_message(di['id_user'],
                                                    di['full_name'],
                                                    pool,
                                                    redis_chat,
                                                    cache)
        sid_pa = get_sid_user(di['id_user'])
        if sid_pa:
            for s in sid_pa:
                await sio.emit(socket_new_message,
                               {'data': data},
                               room=s,
                               namespace=namespace)
        else:
            try:
                # отправка пуша
                send_push_message.delay(di['id_user'], data)
            except:
                traceback.print_exc()
            pass
    return web.json_response({})

app.router.add_post(api_congratulation_user, congratulation_user)

if __name__ == '__main__':
    web.run_app(app)
