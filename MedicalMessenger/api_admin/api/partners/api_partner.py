from flask import request, \
    jsonify
from MedicalMessenger.api_admin.helpers.validators import check_valid_data_admin
from MedicalMessenger.api_admin.server_answers import arguments_error,\
    error_not_valid_phone_or_email,\
    error_create_partner_not_phone,\
    title_email_create_partner,\
    text_email_create_partner,\
    error_update_partner_not_valid_date,\
    title_email_update_password,\
    text_email_update_password
from MedicalMessenger.api_admin.api.partners.db_partner import db_check_exist_email,\
    db_check_exist_phone,\
    db_create_partner,\
    db_get_list_partners,\
    db_update_user,\
    db_get_email
from MedicalMessenger.api_admin.redis_app.redb import remove_user
from MedicalMessenger.config_app import START_CODE_FOR_GOOD,\
    START_CODE_FOR_ERROR,\
    role_partner
import re
import hashlib
from MedicalMessenger.global_functions.send_email import send_mail
from MedicalMessenger.api_admin.helpers.validators import is_valid_email, is_valid_phone
from MedicalMessenger.api_admin.helpers.generators import create_hash_password

@check_valid_data_admin
def get_list_partners():
    data = db_get_list_partners()
    ans_code = START_CODE_FOR_ERROR - data['message']['code']
    if data['message']['code'] > 0:
        ans_code = START_CODE_FOR_GOOD
    # print(data)
    return jsonify(data), ans_code


@check_valid_data_admin
def update_partner(id_partner):
    try:
        # id_partner = request.json['id_partner']
        val = []
        select_update = '''
            UPDATE users
            SET {}
            WHERE id_user = {}
            AND ref_id_role = 2
        '''
        try:
            is_new_password = True
            val.append("password_hash = '{}'".format(create_hash_password(request.json['password'])))
        except KeyError:
            is_new_password = False
        try:
            if len(request.json['full_name']) > 0 and not (request.json['full_name']).isspace():
                val.append("full_name  = '{}'".format(request.json['full_name']))
        except KeyError:
            pass
        try:
            email = request.json['email']
            ans = db_check_exist_email(email)
            if ans == 0 or ans == id_partner:
                if ans == 0:
                    val.append("email = '{}'".format(email))
        except KeyError:
            pass
        if val:
            data = db_update_user(select_update.format(','.join(val), id_partner))
            if data['message']['code'] > 0:
                # print(is_new_password)
                if is_new_password:
                    remove_user(id_partner, role_partner)
                    data_ = db_get_email(id_partner)

                    if data_:
                        send_mail(data_,
                                  title_email_update_password,
                                  text_email_update_password.format(request.json['password']))
                return jsonify(data)
            return jsonify(data), START_CODE_FOR_ERROR - data['message']['code']
        return jsonify({
            'code': 1,
            'message': error_update_partner_not_valid_date,
            'data': {}
        }), START_CODE_FOR_ERROR - error_update_partner_not_valid_date['code']
    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']


@check_valid_data_admin
def create_partner():
    def create_hash_password(password_):
        return hashlib.sha256(bytes(str(password_), 'utf-8')).hexdigest()

    try:
        phone = request.json['phone']
        try:
            email = request.json['email']
            without_mail = False
        except KeyError:
            without_mail = True
        full_name = request.json['full_name']
        password = request.json['password']
        if not without_mail:
            if is_valid_email(email) and is_valid_phone(phone):
                if db_check_exist_phone(phone) == 0:
                    password_hash = create_hash_password(password)
                    if db_check_exist_email(email) == 0:
                        with_mail = True
                        # not exist mail
                        select = '''
                            INSERT INTO users (id_user, ref_id_role, full_name, phone, password_hash, email)
                            VALUES (DEFAULT, 2, '{}', '{}', '{}', '{}' ) returning id_user
                        '''.format(full_name,phone, password_hash, email)
                    else:
                        with_mail = False
                        select = '''
                            INSERT INTO users (id_user, ref_id_role, full_name, phone, password_hash)
                            VALUES (DEFAULT, 2, '{}', '{}', '{}') returning id_user
                        '''.format(full_name, phone, password_hash)
                    data = db_create_partner(select,
                                             with_mail)
                    if data['message']['code'] > 0:
                        if with_mail:
                            send_mail(email, title_email_create_partner, text_email_create_partner.format(password))
                        return jsonify(data), START_CODE_FOR_GOOD
                    return jsonify(data), START_CODE_FOR_ERROR - data['message']['code']
                return jsonify({
                    'code': 1,
                    'message': error_create_partner_not_phone,
                    'data': {}
                }), START_CODE_FOR_ERROR - error_create_partner_not_phone['code']
        else:
            if is_valid_phone(phone):
                if db_check_exist_phone(phone) == 0:
                    password_hash = create_hash_password(password)
                    with_mail = False
                    select = '''
                            INSERT INTO users (id_user, ref_id_role, full_name, phone, password_hash)
                            VALUES (DEFAULT, 2, '{}', '{}', '{}') returning id_user
                        '''.format(full_name, phone, password_hash)
                    data = db_create_partner(select,
                                             with_mail)
                    if data['message']['code'] > 0:
                        return jsonify(data), START_CODE_FOR_GOOD
                    return jsonify(data), START_CODE_FOR_ERROR - data['message']['code']
                return jsonify({
                    'code': 1,
                    'message': error_create_partner_not_phone,
                    'data': {}
                }), START_CODE_FOR_ERROR - error_create_partner_not_phone['code']
        return jsonify({
                'code': 1,
                'message': error_not_valid_phone_or_email,
                'data': {}
            }), START_CODE_FOR_ERROR - error_not_valid_phone_or_email['code']

    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']


