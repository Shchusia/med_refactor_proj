from MedicalMessenger.config_app import path_to_default_photo_profile
from MedicalMessenger.api_admin.settings_app import decorator_connect
from MedicalMessenger.api_admin.server_answers import good_get_partners,\
    good_create_partner,\
    good_create_partner_without_email,\
    good_update_partner


@decorator_connect
def db_get_list_partners(*args, **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_user, full_name, phone, email, path_img
        FROM users
        WHERE ref_id_role = 2
        ORDER BY id_user DESC 
    '''
    cur.execute(select)
    partners = []
    for d in cur.fetchall():
        tmp_data_partner = {
            'id_partner': d[0],
            'name_partner': d[1],
            'phone': d[2],
            'email': d[3]
        }
        if d[4]:
            tmp_data_partner['path_img'] = d[4]
        else:
            tmp_data_partner['path_img'] = path_to_default_photo_profile
        partners.append(tmp_data_partner)
    return {
        'code': 1,
        'message': good_get_partners,
        'data': {
            'partners': partners
        }
    }


@decorator_connect
def db_check_exist_phone(*args, **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    phone = args[0]
    select = '''
        SELECT id_user, phone
        FROM users
        WHERE phone = '{}'
    '''.format(phone)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == phone:
            return 1
    return 0


@decorator_connect
def db_check_exist_email(*args, **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    email = args[0]
    select = '''
        SELECT id_user, email
        FROM users
        WHERE email = '{}'
    '''.format(email)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == email:
            return 1
    return 0


@decorator_connect
def db_create_partner(*args, **kwargs):
    select = args[0]
    # print(select)
    with_mail = args[1]
    conn = kwargs['conn']
    cur = conn.cursor()
    cur.execute(select)
    row = cur.fetchone()[0]
    insert = '''
        INSERT INTO user_data (id_user_data, ref_id_user) 
        VALUES (DEFAULT, {}) 
    ''' .format(row)
    cur.execute(insert)
    conn.commit()
    if with_mail:
        return {
            'code': 1,
            'message': good_create_partner,
            'data': {}
        }
    return {
        'code': 1,
        'message': good_create_partner_without_email,
        'data': {}
    }


@decorator_connect
def db_update_user(*args, **kwargs):
    select = args[0]
    # print(select)
    conn = kwargs['conn']
    cur = conn.cursor()
    cur.execute(select)
    conn.commit()
    return {
        'code': 1,
        'message': good_update_partner,
        'data': {}
    }


@decorator_connect
def db_get_email(*args,
                 **kwargs):
    id_partner = args[0]
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT email
        FROM users
        WHERE id_user = {}
        AND ref_id_role = 2
    '''.format(id_partner)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        return row[0]
    return None