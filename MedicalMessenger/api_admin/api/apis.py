from MedicalMessenger.config_app import domain_api
from MedicalMessenger.api_admin.api.admin.api_admin import get_data_admin, \
    update_admin
from MedicalMessenger.api_admin.api.entry.api_entry import sign_out,sign_in,check_valid_token,restore_password
from MedicalMessenger.api_admin.api.files.api_file import upload_file
from MedicalMessenger.api_admin.api.partners.api_partner import get_list_partners, update_partner,create_partner

api_login = domain_api + '/login'
api_restore_password = domain_api + '/restore_password'
api_logout = domain_api + '/logout'
api_check_valid_token = domain_api + '/check_valid_token'

api_get_list_partners = domain_api + '/partners'
api_create_partner = domain_api + '/partner'
api_update_partner = domain_api + '/partner/<id_partner>'  # post
api_get_partner = domain_api + '/partner/<id_partner>'  # get

api_get_data_admin = domain_api + '/get_data_admin'
api_update_data_admin = domain_api + '/update/data/admin'

api_upload_photo = domain_api + '/upload_file'

routes = [
    (api_get_data_admin, 'get_data', get_data_admin, ['GET']),
    (api_update_data_admin, 'update_admin', update_admin, ['POST']),
    (api_login, 'sign_in_api', sign_in, ['POST']),
    (api_logout, 'sign_out', sign_out, ['GET']),
    (api_check_valid_token, 'check_valid_token', check_valid_token, ['GET']),
    (api_restore_password, 'restore_password', restore_password, ['POST']),
    (api_upload_photo, 'upload_photo', upload_file, ['POST']),
    (api_get_list_partners, 'get_list_partners', get_list_partners, ['GET']),
    (api_update_partner, 'update_partner', update_partner, ['POST']),
    (api_create_partner, 'create_partner', create_partner, ['POST']),
]
