from MedicalMessenger.api_admin.helpers.validators import check_valid_data_admin

from MedicalMessenger.config_app import MEDIA_FOLDER, \
    domain_photo,\
    str_path_to_file, \
    path_to_photo_profile
import binascii
import os
from flask import request,\
    jsonify
from MedicalMessenger.api_admin.server_answers import good_upload_file,\
    arguments_error
from MedicalMessenger.global_functions.compress_photo import comppress_photo


MAX_FILE_SIZE = 1024 * 1024 + 1


@check_valid_data_admin
def upload_file():
    try:

        file = request.files.get('file')
        if not os.path.exists(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile):
            os.makedirs(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile)
        # print(file)
        name = (str(binascii.hexlify(os.urandom(20)))[2:42]) + '.{}'.format(file.filename.split('.')[-1])
        out = open(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile + name, 'wb')
        while True:
            file_bytes = file.read(MAX_FILE_SIZE)
            out.write(file_bytes)
            if len(file_bytes) < MAX_FILE_SIZE:
                break
        out.close()
        comppress_photo(MEDIA_FOLDER + str_path_to_file + path_to_photo_profile + name)
        path = domain_photo + str_path_to_file + path_to_photo_profile + '.'.join(name.split('.')[:-1]) + '.jpeg'
        return jsonify({
                'code': 0,
                'message': good_upload_file,
                'data': {
                    'path_to_file': path
                }
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


