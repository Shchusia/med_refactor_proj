import jwt
from MedicalMessenger.config_app import secret_word,\
    algorithm_encode,\
    role_client, \
    role_admin_app,\
    role_partner,\
    path_to_default_photo_profile
from MedicalMessenger.api_admin.settings_app import decorator_connect
import traceback
from MedicalMessenger.api_admin.server_answers import good_restore_password,\
    sign_in_error_code_or_phone,\
    good_sign_in,\
    restore_error_not_exist,\
    text_email_restore,\
    title_email_restore,\
    good_get_user
import random
import hashlib
from MedicalMessenger.api_admin.celery_.celery_send_email import send_email_restore


def data_to_token(data):
    '''
    формирует токен по данным пользователя json
    :param data: данные пользователя имя пароль id
    :return: возвращает токен для пользователя
    '''

    return jwt.encode(data, secret_word, algorithm=algorithm_encode).decode()


def token_to_data(token):
    '''
    преобразовывает токен в данные пользователя
    :param token: токен который выдавался пользователю
    :return:json с данными пользователя
    '''
    try:
        return True, jwt.decode(token, secret_word, algorithms=[algorithm_encode])
    except jwt.exceptions.DecodeError:
        traceback.print_exc()
        return False, {}


@decorator_connect
def db_get_user(*args,
                **kwargs):
    '''

    :param args: args[0] - id_user
    :param kwargs:
    :return:
    '''

    id_user = args[0]
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT u.id_user, u.phone, u.path_img, u.full_name, u.ref_id_role
        FROM users u
        WHERE u.id_user = {}
    '''.format(id_user)
    # print(select)
    cur.execute(select)
    row = cur.fetchone()
    user = {
        'id_user': row[0],
        'phone': row[1],
        'full_name': row[3],

    }
    select = '''
        SELECT  c.city_title, r.region_
        FROM users u, city c, region r
        WHERE u.id_user = {}
        AND u.ref_id_city = c.id_city
        AND c.ref_id_region = r.id_region
    '''.format(row[0])
    cur.execute(select)
    row_c = cur.fetchone()
    if row_c:
        user['city_title'] = row_c[0],
        user['region'] = row_c[1],
    else:
        user['city_title'] = '',
        user['region'] = '',

    if row[4] == 3:
        user['role'] = role_client
        select = '''
            SELECT specialisation_
            FROM users, specialisation
            WHERE ref_id_specialisation = specialisation.id_specialisation
            AND id_user = {}
        '''.format(id_user)
        cur.execute(select)
        user['specialisation'] = cur.fetchone()[0]
    else:
        if row[4] == 2:
            user['role'] = role_partner
        else:
            user['role'] = role_admin_app
        user['specialisation'] = role_partner
    if row[2]:
        user['path_img'] = row[2]
    else:
        user['path_img'] = path_to_default_photo_profile
    return {
        'code': 1,
        'message': good_get_user,
        'data': {
            'user': user
        }
    }


@decorator_connect
def db_sign_in(*args,
               **kwargs):

    pass


@decorator_connect
def db_check_password(*args,
                      **kwargs):
    '''

    :param args: [0] - phone, [1] - password
    :param kwargs:
    :return:
    '''
    conn = kwargs['conn']
    cur = conn.cursor()
    email = args[0]
    password = args[1]

    select = '''
        SELECT id_user, password_hash, email
        FROM users
        WHERE email = '{}'
        AND password_hash = '{}'
    '''.format(email,
               password)
    # print(select)

    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == password and row[2] == email:
            user_data = db_get_user(row[0])
            if user_data['message']['code'] > 0:
                user = user_data['data']['user']
                token = data_to_token(user)
                return {
                    'code': 1,
                    'message': good_sign_in,
                    'data': {
                        'user': user
                    },
                    'token': token
                }

            raise ValueError
    return {
        'code': 1,
        'message': sign_in_error_code_or_phone,
        'data': {}
    }


@decorator_connect
def db_restore_password(*args,
                        **kwargs):
    def generate_code():
        return ''.join([str(random.randint(0, 9)) for _ in range(0, 6)])

    def create_hash_password(password):
        return hashlib.sha256(bytes(str(password), 'utf-8')).hexdigest()

    email = args[0]
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_user, email
        FROM users
        WHERE email = '{}'
        AND (ref_id_role = 1 OR ref_id_role = 2)
    '''.format(email)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == email:
            new_password = generate_code()
            new_hash = create_hash_password(new_password)
            update = '''
                UPDATE users 
                SET password_hash = '{}'
                WHERE id_user = {}
                AND email = '{}'
            '''.format(new_hash,
                       row[0],
                       email)
            cur.execute(update)
            conn.commit()
            send_email_restore.delay(email, title_email_restore, text_email_restore.format(new_password))
            # send_email_restore(email, title_email_restore, text_email_restore.format(new_password))
            return {
                'code': 1,
                'message': good_restore_password,
                'data': {}

            }
    return {
        'code': 1,
        'message': restore_error_not_exist,
        'data': {}
    }

