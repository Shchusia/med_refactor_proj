from flask import request, \
    jsonify
from MedicalMessenger.api_admin.server_answers import arguments_error,\
    not_valid_token,\
    good_sign_out,\
    error_not_valid_phone_or_email,\
    valid_token
from MedicalMessenger.api_admin.api.entry.db_entry import token_to_data,\
    db_check_password,\
    db_restore_password
from MedicalMessenger.api_admin.redis_app.redb import set_client_multi,\
    check_valid_data_multi,\
    sign_out_multi
from MedicalMessenger.config_app import START_CODE_FOR_GOOD,\
    START_CODE_FOR_ERROR
import traceback

from MedicalMessenger.api_admin.helpers.validators import is_valid_email


def sign_in():
    try:
        email = request.json['email']
        password_hash = request.json['password_hash']
        if is_valid_email(email):
            data = db_check_password(email,
                                     password_hash)
            if data['message']['code'] > 0:
                token = data['token']
                del data['token']
                answer = jsonify(data)
                answer.headers.extend({
                    'Token': token,
                    'Role': data['data']['user']['role']})
                set_client_multi(data['data']['user']['id_user'],
                                 token,
                                 data['data']['user']['role'])
                return answer, START_CODE_FOR_GOOD + data['message']['code']
            return jsonify(data), START_CODE_FOR_ERROR - data['message']['code']
        return jsonify({
            'code': 1,
            'message': error_not_valid_phone_or_email,
            'data': {}
        }), START_CODE_FOR_ERROR - error_not_valid_phone_or_email['code']

    except KeyError:
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']


def check_valid_token():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if check_valid_data_multi(data['id_user'],
                                      token,
                                      data['role']):
                return jsonify({
                    'code': 1,
                    'message': valid_token,
                    'data': {}
                }), START_CODE_FOR_GOOD + valid_token['code']
            else:
                return jsonify({
                    'code': -1,
                    'message': not_valid_token,
                    'data': {}
                }), START_CODE_FOR_ERROR - not_valid_token['code']
        return jsonify({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        }), START_CODE_FOR_ERROR - not_valid_token['code']

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']


def sign_out():
    try:
        token = request.headers['Token']
        is_good, data = token_to_data(token)
        if is_good:
            if sign_out_multi(data['id'],
                              token,
                              data['role']):
                return jsonify({
                    'code': 1,
                    'message': good_sign_out,
                    'data': {}
                })
            else:
                return jsonify({
                    'code': -1,
                    'message': not_valid_token,
                    'data': {}
                })
        return jsonify({
            'code': -1,
            'message': not_valid_token,
            'data': {}
        })
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        })


def restore_password():
    try:
        email = request.json['email']
        data = db_restore_password(email)
        if data['message']['code'] > 0:
            return jsonify(data), START_CODE_FOR_GOOD
        return jsonify(data), START_CODE_FOR_ERROR - data['message']['code']
    except KeyError:
        return jsonify({
            'code': 0,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']



