from MedicalMessenger.config_app import path_to_default_photo_profile
from MedicalMessenger.api_admin.settings_app import decorator_connect
from MedicalMessenger.api_admin.server_answers import error_get_data_admin,\
    good_get_data_admin,\
    good_update_data_admin
from MedicalMessenger.api_admin.api.entry.db_entry import db_get_user
from MedicalMessenger.api_admin.helpers.generators import data_to_token


def get_country_by_phone(phone, cur):
    select = '''
        SELECT id_country, title_country, start_num
        FROM country
    '''
    cur.execute(select)
    for r in cur.fetchall():
        if str(r[2]) == str(phone)[:len(r[2])]:
            return r[0], r[1]
    return 0, None


@decorator_connect
def db_get_cities(*args,
                  **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    phone = args[0]
    id_country, title_country = get_country_by_phone(phone, cur)
    cities = []
    if id_country > 0:
        select = '''
            SELECT id_city, city_title, is_primary_city
            FROM city, region
            WHERE ref_id_country = {}
            AND ref_id_region = id_region
        '''.format(id_country)
        cur.execute(select)
        for r in cur.fetchall():
            cities.append({
                'id_city': r[0],
                'title': r[1]
            })
    return cities


@decorator_connect
def db_get_data_admin(*args,
                      **kwargs):

    id_admin = args[0]
    conn = kwargs['conn']
    cur = conn.cursor()
    select = '''
        SELECT id_user, full_name, path_img, phone, email, ref_id_city
        FROM users
        WHERE ref_id_role = 1
        AND id_user = {}
    '''.format(id_admin)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        data = {
            'id_admin': row[0],
            'full_name': row[1],
            'phone': row[3],
            'email': row[4],
            'id_city': row[5]
        }
        if row[2]:
            data['path_img'] = row[2]
        else:
            data['path_img'] = path_to_default_photo_profile
        cities = db_get_cities(row[3])
        return {
            'code': 1,
            'message': good_get_data_admin,
            'data': {
                'data_admin': data,
                'cities': cities
            }
        }
    return {
        'code': 1,
        'message': error_get_data_admin,
        'data': {}
    }


@decorator_connect
def db_check_exist_email(*args, **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    email = args[0]
    select = '''
        SELECT id_user, email
        FROM users
        WHERE email = '{}'
    '''.format(email)
    cur.execute(select)
    row = cur.fetchone()
    if row:
        if row[1] == email:
            return 1
    return 0


@decorator_connect
def is_exist_city(*args, **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    id_city = args[0]
    select = '''
        SELECT *
        FROM city
        WHERE id_city = {}
    '''.format(id_city)
    cur.execute(select)
    row = cur.fetchone()
    if row():
        return True
    return False


@decorator_connect
def db_update_admin(*args,
                    **kwargs):
    conn = kwargs['conn']
    cur = conn.cursor()
    select = args[0]
    cur.execute(select)
    conn.commit()
    id_user = args[1]

    user_data = db_get_user(id_user)
    if user_data['message']['code'] > 0:
        user = user_data['data']['user']
        token = data_to_token(user)
        return {
            'code': 1,
            'message': good_update_data_admin,
            'data': {
                'user': user
            },
            'token': token
        }

    raise ValueError
