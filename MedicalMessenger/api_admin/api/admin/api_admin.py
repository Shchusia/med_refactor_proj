from MedicalMessenger.api_admin.helpers.validators import check_valid_data_admin
from flask import jsonify, \
    request
from MedicalMessenger.api_admin.api.admin.db_admin import db_get_data_admin,\
    db_check_exist_email,\
    is_exist_city,\
    db_update_admin
from MedicalMessenger.api_admin.server_answers import *
from MedicalMessenger.api_admin.redis_app.redb import set_client_multi
from MedicalMessenger.config_app import START_CODE_FOR_ERROR
import re
import traceback
from MedicalMessenger.api_admin.helpers.validators import token_to_data
import hashlib


def is_valid_email(email):
    reg_exp = r'([\w\d_\.-]+)@([\w\.-]+)(\.[\w\.]+)'
    res = re.match(reg_exp, email)
    if res:
        return True
    return False


@check_valid_data_admin
def get_data_admin():
    try:
        data = request.headers['Token']
        is_valid, data = token_to_data(data)
        answer = db_get_data_admin(data['id_user'])
        if answer['message']['code'] > 0:
            return jsonify(answer)
        return jsonify(answer), START_CODE_FOR_ERROR - data['message']['code']
    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']


@check_valid_data_admin
def update_admin():
    def create_hash_password(password_):
        return hashlib.sha256(bytes(str(password_), 'utf-8')).hexdigest()

    try:
        select = '''
            UPDATE users 
            SET {}
            WHERE id_user = {}
            AND ref_id_role = 1
        '''
        data = request.headers['Token']
        is_valid, data = token_to_data(data)
        full_name = request.json['full_name']
        id_city = request.json['id_city']
        email = request.json['email']
        try:
            path_to_img = request.json['path_to_file']
        except KeyError:
            path_to_img = ''
        try:
            password = request.json['password']
        except KeyError:
            password = ''
        except ValueError:
            password = ''
        res_check_email = db_check_exist_email(email)
        val = []
        if res_check_email == 0 and is_valid_email(email):
            val.append("email = '{}'".format(email))
        if password:
            val.append("password_hash = '{}'".format(create_hash_password(password)))
        if is_exist_city(id_city):
            val.append("ref_id_city = {}".format(id_city))
        if path_to_img:
            val.append("path_img = '{}'".format(path_to_img))
        if full_name:
            val.append("full_name = '{}'".format(full_name))
        if val:
            data = db_update_admin(select.format(','.join(val),
                                                 data['id_user']),
                                   data['id_user'])
            if data['message']['code'] > 0:
                token = data['token']
                del data['token']
                answer = jsonify(data)
                answer.headers.extend({
                    'Token': token,
                    'Role': data['data']['user']['role']})
                set_client_multi(data['data']['user']['id_user'],
                                 token,
                                 data['data']['user']['role'])
                return answer
            return jsonify(data), START_CODE_FOR_ERROR - data['message']['code']
        return jsonify({
            'code': 1,
            'message': error_update_data_admin,
            'data': {}
        }), START_CODE_FOR_ERROR - error_update_data_admin['code']

    except KeyError:
        traceback.print_exc()
        return jsonify({
            'code': 1,
            'message': arguments_error,
            'data': {}
        }), START_CODE_FOR_ERROR - arguments_error['code']


