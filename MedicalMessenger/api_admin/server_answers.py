good_check_phone = {
    'title': 'успех',
    'body': 'телефон есть в системе',
    'code': 1,
}

sign_in_not_exist_phone = {
    'title': '',
    'body': 'нет такого телефона в системе',
    'code': 1
}

good_sign_in = {
    'title': 'успех',
    'body': 'успешный вход',
    'code': 1,
}

good_restore_password = {
    'title': 'успех',
    'body': 'поздравляем, вам на почту выслан новый пароль',
    'code': 2
}

good_get_user = {
    'title': 'успех',
    'body': 'получен пользователь',
    'code': 3
}


good_get_partners = {
    'title': 'успех',
    'body': 'вы успешно получили партенров',
    'code': 4
}

good_create_partner = {
    'title': 'успех',
    'body': 'вы успешно создали партнера',
    'code': 5
}

good_create_partner_without_email = {
    'title': 'успех',
    'body': 'вы успешно создали партнера, но без email-а т.к. указанная почта принадледит другому пользователю',
    'code': 6
}

good_update_partner = {
    'title': 'успех',
    'body': 'вы успешно обновили партнера',
    'code': 4
}

good_sign_out = {
    'title': 'успех',
    'body': 'успешный выход',
    'code': 3,
}

good_upload_file = {
    'title': 'успех',
    'body': 'файл успешно загружен',
    'code': 4,
}

create_coffee_network = {
    'title': 'успех',
    'body': 'сеть создана',
    'code': 5,
}

good_select = {
    'title': 'успех',
    'body': 'запрос успешно выполнен',
    'code': 6,
}

valid_token = {
    'title': 'успех',
    'body': 'ваши данные валидны',
    'code': 7
}

good_get_data_admin = {
    'title': 'успех',
    'body': 'успех, вышло получить данные админа',
    'code': 8
}


good_update_data_admin = {
    'title': 'успех',
    'body': 'вы успешно обновили данные админа',
    'code': 9
}

# ___________________ #

arguments_error = {
    'title': 'ошибка',
    'body': 'нет необходимых аргументов для выполнения запроса',
    'code': -1
}

sign_in_error_not_exist_phone = {
    'title': 'ошибка входа',
    'body': 'нет такого телефона в системе',
    'code': -2
}

sign_in_error_code_or_phone = {
    'title': 'ошибка входа',
    'body': 'неверный пароль и/или email',
    'code': -3
}

restore_error_not_exist = {
    'title': 'ошибка востановления',
    'body': 'нет такой почты или нет прав доступа',
    'code': -4
}


error_create_partner_not_phone = {
    'title': 'ошибка создания партнера',
    'body': 'данный номер принадлежит другому пользователю',
    'code': -5
}

error_update_partner_not_valid_date = {
    'title': 'ошибка обновления',
    'body': 'данные не прошли проверку валидации и уникальности',
    'code': -6
}

error_not_valid_phone_or_email = {
    'title': 'ошибка регистрации',
    'body': 'не валидный телефон или эл.почта',
    'code': -7
}

error_get_data_admin = {
    'title': 'ошибка',
    'body': 'по каки - то неизвестным причинам не вышло получить данные админа',
    'code': -8
}

error_update_data_admin = {
    'title': 'ошибка',
    'body': 'данные не прошли проверку валидации и уникальности',
    'code': -9
}

error_type_category = {
    'title': 'ошибка',
    'body': 'ошибка, нет такой категории',
    'code': -4
}

error_coffee_shop = {
    'title': 'ошибка',
    'body': 'данное заведение вам не принадлежит',
    'code': -5
}

error_in_select = {
    'title': 'ошибка в запросе',
    'body': 'ошибка в запросе к базе данных',
    'code': -31
}

error_in_connect = {
    'title': 'ошибка при коннекте',
    'body': 'ошибка попытке подключиться к базе данных',
    'code': -31
}

not_valid_token = {
    'title': 'ошибка токена',
    'body': 'не валидный токен',
    'code': -30,
}

title_email_restore = 'Восстановление пароля в Medical Messenger'
text_email_restore = 'Ваш новый пароль {}'

title_email_create_partner = 'Регистрация в Medical Messenger'
text_email_create_partner = 'Вас зарегистрировали в Medical Messenger ваш пароль для входа: {}'

title_email_update_password = 'Обновление данных в  Medical Messenger'
text_email_update_password = 'вам изсенили пароль на {}'
