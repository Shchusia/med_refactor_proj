from flask import Flask
from celery import Celery
from MedicalMessenger.config_app import path_to_celery

app = Flask(__name__)

app.config['CELERY_BROKER_URL'] = path_to_celery
app.config['CELERY_RESULT_BACKEND'] = path_to_celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


def init_app(app):
    from MedicalMessenger.api_admin.api.apis import routes
    for route in routes:
        app.add_url_rule(route[0],
                         route[1],
                         route[2],
                         methods=route[3])
    return app


app = init_app(app)


