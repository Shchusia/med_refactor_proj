from MedicalMessenger.config_app import cache


def set_information_user(id_user, data):
    cache.set('information_user_{}'.format(id_user), data)


def get_information_user(id_user):
    return cache.get('information_user_{}'.format(id_user))
