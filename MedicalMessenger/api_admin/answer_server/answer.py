import xlrd
from MedicalMessenger.config_app import MEDIA_FOLDER
from MedicalMessenger.config_app import path_to_answer_api_admin


class AnswerServer:
    default_language = 'ru'

    def parse_file(self, path):

        rb = xlrd.open_workbook(path)
        sheet = rb.sheet_by_index(0)
        r_lang = sheet.row_values(0)[2:]
        voc = {r: {} for r in sheet.row_values(0)[1:]}
        for rownum in range(1, sheet.nrows, 2):
            r_title = sheet.row_values(rownum)
            r_body = sheet.row_values(rownum + 1)
            r_code = r_title[1]
            for i, val in enumerate(r_lang):
                tmp = {
                    'title': r_title[i+2],
                    'body': r_body[i+2],
                    'code': int(r_code) if r_code else 0
                }
                if tmp['title'] != '' and tmp['body'] != '':
                    voc[val][r_title[0]] = tmp
        self.js = voc
        # print(self.js)

    def __init__(self):
        self.js = {}
        self.parse_file(MEDIA_FOLDER + path_to_answer_api_admin + '/language.xls')

    def get_answer_server(self,
                          key,
                          data_for_return={},
                          code=1,
                          language='ru'):
        # print(self.js)
        if self.js.get(language).get(key) and self.js[language][key]['code'] != 0:
            message = self.js[language][key]
        elif self.js.get(self.default_language).get(key):
            message = self.js[self.default_language][key]
        else:
            raise KeyError
        return {
            'code': code,
            'message': message,
            'data': data_for_return,
        }

    def get_only_message(self, key, language='ru'):
        if self.js.get(language).get(key) and self.js[language][key]['code'] != 0:
            message = self.js[language][key]
        else:
            message = self.js[self.default_language][key]
        return message


answer_json = AnswerServer()

