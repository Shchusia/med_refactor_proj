from MedicalMessenger.api_admin import celery
from MedicalMessenger.global_functions.send_email import send_mail


@celery.task
def send_email_restore(to,
                       subject,
                       text):
    send_mail(to,
              subject,
              text)
