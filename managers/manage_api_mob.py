import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from MedicalMessenger.api_mobile_and_sockets.__init__ import app
from aiohttp import web

if __name__ == '__main__':
    web.run_app(app)