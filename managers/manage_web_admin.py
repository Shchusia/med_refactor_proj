import os
import sys
from flask_script import Manager, Server
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from MedicalMessenger.web_admin.__init__ import app
# from HeavenLife.api_admin.__init__ import celery



manager = Manager(app)


manager.add_command("runserver", Server(
    use_debugger=True,
    use_reloader=True,
    host='0.0.0.0',
    port='5021'
    )
)

if __name__ == "__main__":
    manager.run()
